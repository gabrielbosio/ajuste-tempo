# Ajuste del tempo de una pista de audio en base al tempo de una señal de audio externa

## Instalación

1. Instalar [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/v3.3.3/get-started-cmake/index.html) (se recomienda la [versión 3.3.3](https://github.com/espressif/esp-idf/releases/tag/v3.3.3)).

1. Clonar repositorio en la misma carpeta donde se clonó el repositorio de ESP-IDF.

1. Conectar ESP32 vía USB.

1. En la carpeta raíz del proyecto, ejecutar:
`idf.py -p ruta/a/puerto fullclean build flash monitor`
Donde `ruta/a/puerto` es el puerto serial. Por ejemplo, en Linux:
`idf.py -p /dev/ttyUSB0 fullclean build flash monitor`