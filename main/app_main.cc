#include <cstdint>
#include <cstring>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "tarea_ajuste.h"
#include "tarea_reproduccion.h"
#include "CronometroEsp.h"
#include "MedidorLatencia.h"
#include "ImplCodecI2S.h"
#include "LectorAudio.h"
#include "LectorSD.h"
#include "ControladorWiFi.h"
#include "ServidorWeb.h"

extern "C" esp_err_t app_main()
{
  static ControladorWiFi wifi("myssid", "mypassword");
  static uint8_t pista_audio[8192];
  static CronometroEsp cronometro_medidor;
  static MedidorLatencia medidor_latencia(ImplCodecI2S::frecuencia_muestreo,
                                          &cronometro_medidor);
  static LectorAudio lector_audio(pista_audio, sizeof(pista_audio), 4096, 4, 
                                  &medidor_latencia);
  static LectorSD lector_sd(pista_audio, sizeof(pista_audio));
  if (!lector_sd.Iniciado()) {
    return ESP_FAIL;
  }

  static Contexto contexto;
  contexto.lector_sd = &lector_sd;
  contexto.tempo = lector_sd.configuracion()->tempo;
  static Parametros parametros = {
    .lector_audio = &lector_audio,
    .lector_sd = &lector_sd,
    .tempo = &contexto.tempo
  };

  xTaskCreate(tarea_ajuste, "Ajuste", 4096, &parametros, 4,
              &contexto.controlador_ajuste);
  xTaskCreate(tarea_reproduccion, "Reproduccion", 4096, &lector_audio, 5,
              &contexto.controlador_reproduccion);
  
  static ServidorWeb servidor(&contexto);

  return ESP_OK;
}