import argparse
import math


def escribir_valores_tabla_en_archivo(archivo_tabla, longitud):
  dos_pi_sobre_n = 2 * math.pi / longitud
  for i in range(longitud):
    archivo_tabla.write(f"{math.cos(dos_pi_sobre_n * i)}, ")
    archivo_tabla.write(f"{math.sin(dos_pi_sobre_n * i)}, ")
    if i > 0 and i % 4 == 0:
      archivo_tabla.write("\n")


def escribir_en_archivo_tabla(longitud):
  with open("../main/tabla_senos.h", "w") as archivo_tabla:
    archivo_tabla.write("#ifndef TABLA_SENOS_H\n")
    archivo_tabla.write("#define TABLA_SENOS_H\n\n")
    archivo_tabla.write("const float tabla_senos[] = {\n")
    escribir_valores_tabla_en_archivo(archivo_tabla, longitud)
    archivo_tabla.write("\n};\n\n")
    archivo_tabla.write("#endif\n")


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("longitud", metavar="N", type=int)
  argumentos = parser.parse_args()
  escribir_en_archivo_tabla(argumentos.longitud)
