#ifndef TAREA_AJUSTE_H
#define TAREA_AJUSTE_H

#include "LectorAudio.h"
#include "LectorSD.h"

/**
 * Conjunto de parametros de tarea_ajuste.
*/
struct Parametros {
  /**
   * Este lector debe ser el mismo que el usado como parametro de 
   * tarea_reproduccion.
  */
  LectorAudio* lector_audio;
  /**
   * Este lector debe ser el mismo que el asociado a Contexto.
  */
  LectorSD* lector_sd;
  /**
   * Puntero al tempo actual de la pista de audio.
  */
  float* tempo;
};

/**
 * Tarea de ajuste de tempo de la pista de audio. 
 * Para crear esta tarea, llamar a la funcion xTaskCreate de la biblioteca 
 * FreeRTOS con esta funcion como argumento.
 * @param parametros El parametro que recibe esta tarea debe ser un objeto 
 * Parametros.
*/
void tarea_ajuste(void* parametros);

#endif