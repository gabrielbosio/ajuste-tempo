#include <cstdio>
#include <cstring>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "CronometroEsp.h"
#include "GeneradorDelayFreeRTOS.h"
#include "ImplControladorADC.h"
#include "DetectorPico.h"
#include "DetectorTempo.h"
#include "GestorVentanas.h"
#include "TrFourier.h"
#include "PhaseVocoder.h"
#include "EscaladorTiempo.h"
#include "tabla_senos.h"
#include "tarea_ajuste.h"

void tarea_ajuste(void* parametros)
{
  auto p = (Parametros*)parametros;
  LectorAudio* lector_audio = p->lector_audio;
  size_t longitud_buffer_ratio_uno = lector_audio->longitud_buffer() / 2;
  auto cronometro = new CronometroEsp();
  auto generador_delay = new GeneradorDelayFreeRTOS();
  LectorSD* lector_sd = p->lector_sd;
  if (!lector_sd->Leer()) {
    vTaskDelete(nullptr);
  }
  auto adc = new ImplControladorADC();
  const float intensidad_pico = 0.99F;
  const size_t cantidad_maxima_picos = 8UL;
  int picos[cantidad_maxima_picos];
  auto detector_pico = new DetectorPico(intensidad_pico, picos,
                                        cantidad_maxima_picos);
  Configuracion* configuracion = p->lector_sd->configuracion();
  auto detector_tempo = new DetectorTempo(configuracion->tempo,
                                          &configuracion->umbral_minimo,
                                          configuracion->umbral_polirritmico,
                                          configuracion->longitud_umbral,
                                          picos, cantidad_maxima_picos);
  const size_t longitud_ventana = 512UL;
  const size_t longitud_tabla = sizeof(tabla_senos) / sizeof(float);
  auto gestor_ventanas = new GestorVentanas(longitud_ventana, tabla_senos,
                                            longitud_tabla);
  auto fft = new TrFourier(longitud_ventana, tabla_senos, longitud_tabla,
                           TipoFFT::real);
  auto phase_vocoder = new PhaseVocoder(tabla_senos, longitud_tabla);
  auto escalador = new EscaladorTiempo(lector_audio->longitud_buffer(),
                                       gestor_ventanas, fft, phase_vocoder);
  auto escalado = new float[lector_audio->longitud_buffer()];
  float ratio = 1.0F;
  int instante_muestra = 0;
  float* tempo = p->tempo;
  
  while (true) {
    if (lector_audio->leyo_toda_la_pista()) {
      if (lector_sd->Leer()) {
        lector_audio->NotificarCargaNuevaPista();
      } else {
        vTaskDelete(nullptr);
      }
    }
    float muestra = adc->Leer();
    instante_muestra += cronometro->Finalizar() / 1000;
    cronometro->Comenzar();
    bool detector_listo = detector_pico->Ejecutar(muestra, instante_muestra);
    if (detector_listo) {
      *tempo = detector_tempo->Ejecutar(detector_pico->cantidad_picos());
      std::printf("Tempo inicial: %f Tempo: %f Umbral minimo: %d "
                  "Umbral polirritmico: ",
                  configuracion->tempo, *tempo, configuracion->umbral_minimo);
      for (int j = 0; j < configuracion->longitud_umbral; j++) {
        std::printf("%d ", configuracion->umbral_polirritmico[j]);
      }
    }

    if (!lector_audio->buffer_entrada_listo()) {
      if (*tempo > 0.0F) {
        float nuevo_ratio = configuracion->tempo / *tempo;
        if (nuevo_ratio >= 0.5F && nuevo_ratio <= 2.0F) {
          ratio = nuevo_ratio;
        }
      }
      size_t longitud_escalado = longitud_buffer_ratio_uno * ratio;
      std::memset(escalado, 0, longitud_escalado * sizeof(float));
      escalador->Ejecutar(escalado, lector_audio->buffer_entrada_actual(),
                          longitud_escalado, longitud_buffer_ratio_uno);
      lector_audio->LlenarBufferSalida(escalado, longitud_escalado);
    }

    generador_delay->Esperar(10);
  }

  delete cronometro;
  delete generador_delay;
  delete lector_sd;
  delete adc;
  delete detector_pico;
  delete detector_tempo;
  delete gestor_ventanas;
  delete fft;
  delete phase_vocoder;
  delete escalador;
  delete[] escalado;
  vTaskDelete(nullptr);
}