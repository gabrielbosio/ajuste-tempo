#ifndef LECTOR_SD_H
#define LECTOR_SD_H

#include <cstddef>
#include <cstdint>
#include <driver/sdmmc_host.h>
#include <driver/sdspi_host.h>
#include "esp_vfs_fat.h"
#include "Desempaquetador.h"

/**
 * Capa de abstraccion del modulo de lectura y escritura de tarjetas SD de 
 * ESP-IDF.
*/
class LectorSD {
public:
  /**
   * Activa la funcionalidad de lectura y escritura de tarjetas SD. 
   * Supone que la tarjeta SD tiene dos archivos: 
   * config.txt donde se almacena la configuracion y pista.txt donde se 
   * almacena para la pista de audio. 
   * Copia la configuracion almacenada en la tarjeta SD en un objeto 
   * Configuracion.
   * @param pista_audio Buffer en donde copiar la pista de audio almacenada 
   * en la tarjeta SD.
   * @param longitud_pista Longitud del buffer.
  */
  LectorSD(uint8_t* pista_audio, size_t longitud_pista);
  /**
   * Desactiva la funcionalidad de lectura y escritura de tarjetas SD.
  */
  ~LectorSD();
  /**
   * Copia en pista_audio() una cantidad de bytes de la pista de audio 
   * almacenada en la tarjeta SD dada por longitud_pista(). Avanza el puntero 
   * de lectura de la pista de audio una cantidad de bytes dada por 
   * longitud_pista(). Si el puntero llega al final de la pista, vuelve al 
   * inicio.
   * @return true si este lector puede leer la pista de audio de la tarjeta 
   * SD. De lo contrario, false.
  */
  bool Leer();
  /**
   * Retorna true si este lector puede leer la pista de audio de la tarjeta 
   * SD. De lo contrario, retorna false.
  */
  bool Iniciado();
  /**
   * Establece el archivo de configuracion como archivo a escribir. Luego de 
   * la llamada a este metodo, este lector no podra leer de la pista de audio 
   * hasta la proxima llamada a FinalizarEscritura().
   * @return true si previo a esta llamada, este lector puede leer la pista 
   * de audio de la tarjeta SD. De lo contrario, false.
  */
  bool IniciarEscritura();
  /**
   * Si la llamada a este metodo es la primera luego de llamar a 
   * IniciarEscritura(), copia los primeros bytes de pista_audio() en el 
   * archivo de configuracion almacenado en la tarjeta SD, y copia el resto 
   * en la pista de audio almacenada en la tarjeta SD. 
   * Las proximas llamadas a este metodo antes de una nueva llamada a 
   * IniciarEscritura() agregan a la pista de audio almacenada en la tarjeta 
   * SD todo el contenido de pista_audio().
   * @return true si la escritura tanto en el archivo de configuracion como en 
   * la pista de audio almacenada en la tarjeta SD fue exitosa. De lo 
   * contrario, false.
  */
  bool Escribir(size_t longitud);
  /**
   * Habilita nuevas lecturas de la pista de audio almacenada en la tarjeta 
   * SD.
   * @return true si este lector puede leer la pista de audio de la tarjeta 
   * SD. De lo contrario, false.
  */
  bool FinalizarEscritura();
  /**
   * Buffer donde se almacena la pista de audio leida de la tarjeta SD o 
   * se almacena la pista de audio a escribir en la tarjeta SD.
  */
  uint8_t* pista_audio();
  /**
   * Longitud del buffer donde se almacena la pista de audio.
  */
  size_t longitud_pista();
  /**
   * Configuracion originaria del archivo de configuracion o destinada al 
   * archivo de configuracion de la tarjeta SD.
  */
  Configuracion* configuracion();
private:
  uint8_t* pista_audio_;
  size_t longitud_pista_;
  Desempaquetador* desempaquetador;
  sdmmc_host_t host;
  sdspi_slot_config_t config_slot;
  static const gpio_num_t pin_miso = GPIO_NUM_33;
  static const gpio_num_t pin_mosi = GPIO_NUM_32;
  static const gpio_num_t pin_clk = GPIO_NUM_27;
  static const gpio_num_t pin_cs = GPIO_NUM_14;
  sdmmc_card_t tarjeta;
  BYTE numero_unidad = 0xFF;
  char unidad[3] = {(char)('0' + numero_unidad), ':', '\0'};
  FATFS* sistema_archivos;
  FILE* archivo_pista;
  FILE* archivo_config;
  bool se_guardo_configuracion = false;
  static const size_t longitud_configuracion = 5;
  Configuracion configuracion_;
  void InicializarTarjeta();
  void MontarParticion();
  bool LeerConfiguracion();
  bool EscribirConfiguracion(uint8_t** pista_audio);
};

#endif