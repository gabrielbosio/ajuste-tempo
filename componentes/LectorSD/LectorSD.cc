#include <iostream>
#include <sdmmc_cmd.h>
#include <diskio.h>
#include "LectorSD.h"

LectorSD::LectorSD(uint8_t* pista_audio, size_t longitud_pista) :
  pista_audio_(pista_audio),
  longitud_pista_(longitud_pista),
  desempaquetador(new Desempaquetador())
{
  InicializarTarjeta();
  MontarParticion();
  
  bool lectura_exitosa = LeerConfiguracion();
  if (lectura_exitosa) {
    std::cout << "Tempo: " << configuracion_.tempo << "\n";
    archivo_pista = fopen("/sdcard/pista.txt", "r");
    if (!Iniciado()) {
      std::cout << "No se encontro el archivo en la tarjeta.\n";
    }
  }
}

LectorSD::~LectorSD()
{
  fclose(archivo_pista);
  f_mount(nullptr, unidad, 0);
  ff_diskio_unregister(numero_unidad);
  (*tarjeta.host.deinit)();
  esp_vfs_fat_unregister_path("/sdcard");
  std::cout << "Tarjeta desmontada.\n";

  delete desempaquetador;
}

bool LectorSD::Leer()
{
  if (!Iniciado()) {
    return false;
  }
  size_t bytes_leidos = fread(pista_audio_, sizeof(uint8_t), longitud_pista_,
                              archivo_pista);
  if (bytes_leidos != longitud_pista_) {
    if (feof(archivo_pista)) {
      rewind(archivo_pista);
      fread(pista_audio_ + bytes_leidos, sizeof(uint8_t),
            longitud_pista_ - bytes_leidos, archivo_pista);
    } else if (ferror(archivo_pista)) { 
      perror("No se pudo leer el archivo");
      return false;
    }
  }
  return true;
}

bool LectorSD::Iniciado() { return archivo_pista != nullptr; }

bool LectorSD::IniciarEscritura()
{
  if (!Iniciado()) {
    return false;
  }
  fclose(archivo_pista);
  se_guardo_configuracion = false;
  archivo_config = fopen("/sdcard/config.txt", "w");
  if (archivo_config == nullptr) {
    std::cout << "No se pudo iniciar la escritura del archivo.\n";
    return false;
  }
  return true;
}

bool LectorSD::Escribir(size_t longitud)
{
  if (longitud > longitud_pista_) {
    longitud = longitud_pista_;
  }

  if (!se_guardo_configuracion) {
    bool escritura_exitosa = EscribirConfiguracion(&pista_audio_);
    if (!escritura_exitosa) {
      return false;
    }
    se_guardo_configuracion = true;
    longitud -= longitud_configuracion;
    fclose(archivo_config);
    std::cout << "Nuevo tempo: " << configuracion_.tempo << "\n";
    std::cout << "Nuevo umbral minimo: " << pista_audio_[3] << "\n";
    std::cout << "Nuevo umbral polirritmico: ";
    for (int i = 0; i < configuracion_.longitud_umbral; i++) {
      std::cout << configuracion_.umbral_polirritmico[i] << " ";
    }
    std::cout << "\n";
    archivo_pista = fopen("/sdcard/pista.txt", "w");
  }
  
  size_t bytes_escritos = fwrite(pista_audio_, sizeof(uint8_t), longitud,
                                 archivo_pista);
  if (bytes_escritos != longitud_pista_ && ferror(archivo_pista)) {
    perror("No se pudo escribir la pista en el archivo");
    return false;
  }
  return true;
}

bool LectorSD::FinalizarEscritura()
{
  if (!Iniciado()) {
    return false;
  }
  fclose(archivo_pista);
  archivo_pista = fopen("/sdcard/pista.txt", "r");
  if (!Iniciado()) {
    std::cout << "No se pudo retomar la lectura del archivo.\n";
    return false;
  }
  return true;
}

uint8_t* LectorSD::pista_audio() { return pista_audio_; }

size_t LectorSD::longitud_pista() { return longitud_pista_; }

Configuracion* LectorSD::configuracion() { return &configuracion_; }

void LectorSD::InicializarTarjeta()
{
  std::cout << "Inicializando tarjeta SD (SPI).\n";
  host = SDSPI_HOST_DEFAULT();
  config_slot = SDSPI_SLOT_CONFIG_DEFAULT();
  config_slot.gpio_miso = pin_miso;
  config_slot.gpio_mosi = pin_mosi;
  config_slot.gpio_sck = pin_clk;
  config_slot.gpio_cs = pin_cs;
}

void LectorSD::MontarParticion()
{
  esp_vfs_fat_sdmmc_mount_config_t config_montaje = {
    .format_if_mount_failed = false,
    .max_files = 5,
    .allocation_unit_size = 0
  };

  ff_diskio_get_drive(&numero_unidad);
  (*host.init)();
  sdspi_host_init_slot(host.slot, (const sdspi_slot_config_t*)&config_slot);
  sdmmc_card_init(&host, &tarjeta);
  ff_diskio_register_sdmmc(numero_unidad, &tarjeta);
  unidad[0] = '0' + numero_unidad;
  esp_vfs_fat_register("/sdcard", unidad, config_montaje.max_files,
                       &sistema_archivos);
  f_mount(sistema_archivos, unidad, 1);
  sdmmc_card_print_info(stdout, &tarjeta);
}

bool LectorSD::LeerConfiguracion()
{
  archivo_config = fopen("/sdcard/config.txt", "r");
  if (archivo_config == nullptr) {
    std::cout << "No se pudo abrir el archivo de configuracion.\n";
    return false;
  }
  uint8_t buffer_configuracion[longitud_configuracion];
  size_t bytes_leidos = fread(buffer_configuracion, sizeof(uint8_t),
                              longitud_configuracion, archivo_config);
  if (bytes_leidos != longitud_pista_ && ferror(archivo_config)) {
    perror("No se pudo leer el archivo de configuracion");
    return false;
  }
  configuracion_.pista = nullptr;
  desempaquetador->Desempaquetar(buffer_configuracion, longitud_configuracion,
                                 &configuracion_);
  fclose(archivo_config);
  return true;
}

bool LectorSD::EscribirConfiguracion(uint8_t** pista_audio)
{
  bool exito = desempaquetador->Desempaquetar(pista_audio_, longitud_pista_,
                                              &configuracion_);
  if (!exito) {
    std::cout << "Error de formato de la trama recibida.\n";
    return false;
  }
  size_t bytes_escritos = fwrite(pista_audio_, sizeof(uint8_t),
                                 longitud_configuracion, archivo_config);
  if (bytes_escritos != longitud_configuracion && ferror(archivo_config)) {
    perror("No se pudo escribir el archivo de configuracion");
    return false;
  }
  return true;
}