#include "ImplCodecI2S.h"

ImplCodecI2S::ImplCodecI2S(BitsMuestreo bits_muestreo)
  : bits_muestreo_(bits_muestreo)
{
  i2s_bits_per_sample_t muestreo_config = I2S_BITS_PER_SAMPLE_16BIT;
  if (bits_muestreo_ != BitsMuestreo::bits_16) {
    muestreo_config = I2S_BITS_PER_SAMPLE_32BIT;
  }

  i2s_config_t i2s_config = {
    .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX |
                         I2S_MODE_DAC_BUILT_IN),
    .sample_rate =  frecuencia_muestreo,
    .bits_per_sample = muestreo_config,
    .channel_format = formato_canal,
    .communication_format = I2S_COMM_FORMAT_I2S_MSB,
    .intr_alloc_flags = 0,
    .dma_buf_count = 2,
    .dma_buf_len = 1024,
    .use_apll = 1,
  };
  i2s_driver_install(puerto, &i2s_config, 0, nullptr);
  i2s_set_dac_mode(I2S_DAC_CHANNEL_BOTH_EN);
  buffer_escritura = (uint8_t*) calloc(longitud_maxima_escritura,
                                       sizeof(uint8_t));
}

ImplCodecI2S::~ImplCodecI2S()
{
  i2s_driver_uninstall(puerto);
  free(buffer_escritura);
  buffer_escritura = nullptr;
}

void ImplCodecI2S::Escribir(float* origen, size_t longitud, int offset)
{

  size_t longitud_escritura =
    CopiarContenidoEscalado(buffer_escritura, origen, longitud, offset);
  size_t b;
  i2s_write(puerto, buffer_escritura, longitud_escritura, &b, portMAX_DELAY);
}

size_t ImplCodecI2S::CopiarContenidoEscalado(uint8_t* destino, float* origen,
                                             int longitud,int offset)
{
  uint32_t j = 0;
  float* buffer_origen = origen + offset;
  if (bits_muestreo_ == BitsMuestreo::bits_16) {
    for (int i = 0; i < longitud; i++) {
      destino[j++] = 0;
      destino[j++] = (uint8_t)(buffer_origen[i] + 127.0);
    }
    return longitud * 2;
  } else {
    for (int i = 0; i < longitud; i++) {
        destino[j++] = 0;
        destino[j++] = 0;
        destino[j++] = 0;
        destino[j++] = (uint8_t)(buffer_origen[i] + 127.0);
    }
    return longitud * 4;
  }
}
