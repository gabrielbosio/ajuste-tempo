#ifndef IMPL_CODEC_I2S_H
#define IMPL_CODEC_I2S_H

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/i2s.h>
#include "CodecI2S.h"

enum class BitsMuestreo {
  bits_16 = I2S_BITS_PER_SAMPLE_16BIT,
  bits_32 = I2S_BITS_PER_SAMPLE_32BIT
};

/**
 * Capa de abstraccion del modulo de escritura via I2S de ESP-IDF.
*/
class ImplCodecI2S : public CodecI2S {
public:
  /**
   * Activa la funcionalidad de escritura via I2S.
   * El periferico I2S se enruta al DAC del ESP32.
  */
  ImplCodecI2S(BitsMuestreo bits_muestreo = BitsMuestreo::bits_16);
  ~ImplCodecI2S() override;
  void Escribir(float* origen, size_t longitud, int offset) override;
  static const int frecuencia_muestreo = 16000;
private:
  BitsMuestreo bits_muestreo_;
  static const i2s_port_t puerto = I2S_NUM_0;
  static const i2s_channel_fmt_t formato_canal = I2S_CHANNEL_FMT_ONLY_RIGHT;
  uint8_t* buffer_escritura;
  static const size_t longitud_maxima_escritura = 16 * 1024;
  size_t CopiarContenidoEscalado(uint8_t* buffer_destino, float* contenido,
                                 int longitud, int offset);
};

#endif