#ifndef CODEC_I2S_H
#define CODEC_I2S_H

#include <cstddef>

/**
 * Interfaz de la capa de abstraccion del modulo de escritura via I2S.
*/
class CodecI2S {
public:
  /**
   * Desactiva la funcionalidad de escritura via I2S.
  */
  virtual ~CodecI2S() {};
  /**
   * Envia al periferico I2S los datos de origen, tomando origen[offset] como 
   * primer byte y origen[longitud - 1] como ultimo byte.
   * @param origen Buffer de datos a enviar.
   * @param longitud Longitud del buffer.
   * @param offset Indice del primer byte a enviar.
  */
  virtual void Escribir(float* origen, size_t longitud, int offset) = 0;
};

#endif