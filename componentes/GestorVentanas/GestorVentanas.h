#ifndef GESTOR_VENTANAS_H
#define GESTOR_VENTANAS_H

#include <cstddef>
#include <cstdint>

/**
 * Gestor de ventanas de audio. Usar con un modulo de FFT para calcular la 
 * STFT.
*/
class GestorVentanas {
public:
  /**
   * Crea un gestor de ventanas. 
   * Este gestor divide un array de muestras de audio en ventanas de audio 
   * usando tabla_senos para ejecutar funciones trigonometricas de forma 
   * eficiente.
   * @param longitud_ventana Longitud de cada ventana de audio.
   * @param tabla_senos Array de valores precalculados de las funciones seno 
   * y coseno. Este gestor supone tabla_senos[i * 2] = cosf(i * N) y 
   * tabla_senos[i * 2 + 1] = sinf(i * N), donde 
   * N = 2 * M_PI / (longitud_tabla / 2).
   * @param longitud_tabla Longitud de tabla_senos.
  */
  GestorVentanas(size_t longitud_ventana, const float* tabla_senos,
                 size_t longitud_tabla);
  /**
   * Copia origen en destino, tomando 
   * origen[indice * (longitud_ventana() - salto())] como primer valor de 
   * origen. Multiplica el array copiado por una ventana de Hann.
   * @param destino Donde se va a almacenar el array procesado. La longitud 
   * de destino debe ser longitud_ventana().
   * @param origen Array a procesar.
   * @param longitud_origen Longitud de origen.
   * @param indice Numero de ventana, siendo 0 la ventana que afecta a los 
   * primeros valores de origen.
  */
  void AplicarVentana(float* destino, uint8_t* origen, size_t longitud_origen,
                      int indice);
  /**
   * Copia origen en destino, tomando 
   * destino[indice * (longitud_ventana() - salto())] como primer valor de 
   * destino. Multiplica el array copiado por una ventana de Hann.
   * @param destino Donde se va a almacenar el array procesado.
   * @param origen Array a procesar. La longitud de origen debe ser 
   * longitud_ventana().
   * @param longitud_destino Longitud de destino.
   * @param indice Numero de ventana, siendo 0 la ventana que afecta a los 
   * primeros valores de destino.
  */
  void UnirAudio(float* destino, float* origen, size_t longitud_destino,
                 int indice);
  /**
   * Divide array por el cuadrado de la ventana Hann.
   * @param array Array a procesar. Este gestor modifica el array, no genera 
   * una copia.
   * @param longitud Longitud de array.
  */
  void Normalizar(float* array, size_t longitud);
  /**
   * Longitud de una ventana de audio.
  */
  size_t longitud_ventana();
  /**
   * Cantidad de muestras que toma la funcion ventana en llegar a su maximo.
  */
  size_t salto();
private:
  size_t longitud_ventana_;
  size_t salto_;
  const float* tabla_senos;
  size_t longitud_tabla;
  float Ventana(int n);
};

#endif