#include "GestorVentanas.h"

GestorVentanas::GestorVentanas(size_t longitud_ventana,
                               const float* tabla_senos,
                               size_t longitud_tabla) :
  longitud_ventana_(longitud_ventana),
  salto_(longitud_ventana / 2),
  tabla_senos(tabla_senos),
  longitud_tabla(longitud_tabla) {}

void GestorVentanas::AplicarVentana(float* destino, uint8_t* origen,
                                    size_t longitud_origen, int indice)
{
  int offset = indice * (longitud_ventana_ - salto_);
  for (int i = 0; i < longitud_ventana_; i++) {
    int ind_origen = i + offset;
    if (ind_origen < salto_) {
      ind_origen = salto_ - ind_origen;
    } else if (ind_origen < salto_ + longitud_origen) {
      ind_origen -= salto_;
    } else {
      ind_origen = longitud_origen - 1 -
                   (ind_origen - salto_ - longitud_origen + 1);
    }
    float valor_origen = origen[ind_origen] - UINT8_MAX / 2;
    destino[i] = valor_origen * Ventana(i);
  }
}

void GestorVentanas::UnirAudio(float* destino, float* origen,
                               size_t longitud_destino, int indice)
{
  int offset = indice * (longitud_ventana_ - salto_);
  for (int i = 0; i < longitud_ventana_; i++) {
    int ind_destino = i + offset - salto_;
    if (ind_destino >= 0 && ind_destino < longitud_destino) {
      destino[ind_destino] += origen[i] * Ventana(i);
    }
  }
}

void GestorVentanas::Normalizar(float* array, size_t longitud)
{
  for (int i = 0; i < longitud; i++) {
    float ventana_cuadrado = Ventana(i - salto_) * Ventana(i - salto_);
    if (i % longitud_ventana_ <= salto_) {
      ventana_cuadrado += Ventana(i) * Ventana(i);
    } else if (i % longitud_ventana_ >= longitud_ventana_ - salto_) {
      ventana_cuadrado += Ventana(i - salto_ * 2) * Ventana(i - salto_ * 2);
    }
    if (ventana_cuadrado > 0.0F) {
      array[i] /= ventana_cuadrado;
    }
  }
}

size_t GestorVentanas::longitud_ventana() { return longitud_ventana_; }

size_t GestorVentanas::salto() { return salto_; }

float GestorVentanas::Ventana(int i)
{
  int salto_tabla = longitud_tabla / longitud_ventana_;
  int m = (i * salto_tabla - longitud_tabla / 2) % longitud_tabla;
  return (1 + tabla_senos[m]) / 2.0F;
}