#ifndef CRONOMETRO_ESP_H
#define CRONOMETRO_ESP_H

#include "Cronometro.h"

/**
 * Capa de abstraccion del modulo de temporizador de ESP-IDF
*/
class CronometroEsp : public Cronometro {
public:
  CronometroEsp();
  ~CronometroEsp() override;
  /**
   * Comienza el conteo de este cronometro. 
   * Si este cronometro ya comenzo su conteo, reinicia ese conteo.
  */
  void Comenzar() override;
  /**
   * Finaliza el conteo de este cronometro.
   * @return El tiempo transcurrido, en microsegundos, entre el comienzo del 
   * conteo y su fin, o 0 si este cronometro no comenzo su conteo.
  */
  int64_t Finalizar() override;
private:
  int64_t t_inicio = 0;
};

#endif