#ifndef CRONOMETRO_H
#define CRONOMETRO_H

#include <cstdint>

/**
 * Interfaz para la capa de abstraccion del modulo de temporizador.
*/
class Cronometro {
public:
  virtual ~Cronometro() {}
  /**
   * Comienza el conteo de este cronometro. 
   * Si este cronometro ya comenzo su conteo, reinicia ese conteo.
  */
  virtual void Comenzar() = 0;
  /**
   * Finaliza el conteo de este cronometro.
   * @return El tiempo transcurrido, en microsegundos, entre el comienzo del 
   * conteo y su fin, o 0 si este cronometro no comenzo su conteo.
  */
  virtual int64_t Finalizar() = 0;
};

#endif