#include <esp_timer.h>
#include "CronometroEsp.h"

CronometroEsp::CronometroEsp() {}

CronometroEsp::~CronometroEsp() {}

void CronometroEsp::Comenzar() {
  t_inicio = esp_timer_get_time();
}

int64_t CronometroEsp::Finalizar() {
  if (t_inicio == 0) {
    return 0;
  }
  int64_t t_final = esp_timer_get_time();
  int64_t dt = t_final - t_inicio;
  t_inicio = 0;
  return dt;
}