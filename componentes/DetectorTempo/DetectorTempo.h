#ifndef DETECTOR_TEMPO_H
#define DETECTOR_TEMPO_H

#include <cstddef>

/**
 * Detector de tempo en base al calculo de la frecuencia de aparicion de 
 * picos en el tiempo.
*/
class DetectorTempo {
public:
  /**
   * Crea un detector de tempo. 
   * Este detector se ejecuta comparando los instantes de tiempo asociados al 
   * buffer de picos pasados como argumento de este constructor y los 
   * instantes de tiempo asociados a un tren de picos generado a un tempo 
   * definido. Este tempo se inicializa con tempo_inicial. Luego de cada 
   * ejecucion del detector, este tempo se actualiza con el resultado de la 
   * ejecucion.
   * @param tempo_inicial Tempo a comparar en la primera deteccion de tempo.
   * @param umbral_minimo Puntero al valor de diferencia minima entre el 
   * instante asociado a un pico almacenado en el buffer de picos y el 
   * instante asociado al pico de el tren de picos generado a un tempo 
   * definido por tempo_inicial o por las ejecuciones del detector.
   * @param umbral_polirritmico Conjunto de factores n que multiplican a una  
   * distancia entre picos d. Si este detector calcula una distancia entre 
   * picos n * d, este detector no tendra en cuenta esa distancia a la hora 
   * de calcular el tempo.
   * @param longitud_umbral Cantidad de factores que conforman 
   * umbral_polirritmico
   * @param picos Buffer de instantes de tiempo, en milisegundos, asociados 
   * a picos de intensidad.
   * @param cantidad_maxima_picos Longitud del buffer picos.
  */
  DetectorTempo(float tempo_inicial, int* umbral_minimo,
                int* umbral_polirritmico, size_t longitud_umbral,
                int* picos, size_t cantidad_maxima_picos);
  /**
   * Dado el buffer de instantes de tiempo pasado como argumento del 
   * constructor, calcula el tempo del conjunto de picos asociados a esos 
   * instantes, es decir, la frecuencia de aparicion de esos picos por minuto.
   * @param cantidad_picos Cantidad de instantes de tiempo almacenados en el 
   * buffer de instantes de tiempo.
   * @return El tempo del conjunto de picos asociados a los instantes de 
   * tiempo almacenados en el buffer pasado como argumento del constructor, o 
   * un numero negativo si no hay instantes almacenados en el buffer.
  */
  float Ejecutar(size_t cantidad_picos);
private:
  float tempo;
  int* umbral_minimo;
  int* umbral_polirritmico;
  size_t longitud_umbral;
  int* picos;
  size_t cantidad_maxima_picos;
  bool EsLaMismaDistancia(int nueva_distancia_picos,
                          int distancia_picos_actual);
};

#endif