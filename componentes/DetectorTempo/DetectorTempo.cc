#include <cstdlib>
#include "DetectorTempo.h"

DetectorTempo::DetectorTempo(float tempo_inicial, int* umbral_minimo,
                             int* umbral_polirritmico,
                             size_t longitud_umbral, int* picos,
                             size_t cantidad_maxima_picos) :
  tempo(tempo_inicial),
  umbral_minimo(umbral_minimo),
  umbral_polirritmico(umbral_polirritmico),
  longitud_umbral(longitud_umbral),
  picos(picos),
  cantidad_maxima_picos(cantidad_maxima_picos) {}

float DetectorTempo::Ejecutar(size_t cantidad_picos)
{
  if (cantidad_picos > cantidad_maxima_picos) {
    cantidad_picos = cantidad_maxima_picos;
  } else if (cantidad_picos <= 1) {
    return -1.0F;
  }

  int minuto_en_ms = 60 * 1000;
  int distancia_picos_actual = 1.0F / tempo * minuto_en_ms;
  int total_distancias = 0;

  for (int i = 0; i < cantidad_picos - 1; i++) {
    int distancia_picos = picos[i + 1] - picos[i];
    if (EsLaMismaDistancia(distancia_picos, distancia_picos_actual)) {
      distancia_picos = distancia_picos_actual;
    }
    total_distancias += distancia_picos;
  }
  
  int distancia_promedio = total_distancias / (cantidad_picos - 1);
  tempo = (float)minuto_en_ms / distancia_promedio;
  
  return tempo;
}

bool DetectorTempo::EsLaMismaDistancia(int nueva_distancia_picos,
                                       int distancia_picos_actual)
{
  bool resultado = abs(nueva_distancia_picos - distancia_picos_actual) <=
                   *umbral_minimo;
  for (int i = 0; i < longitud_umbral; i++) {
    if (umbral_polirritmico[i] == 0) {
      continue;
    }
    resultado |= abs(nueva_distancia_picos - distancia_picos_actual *
                     umbral_polirritmico[i]) <= *umbral_minimo;
    resultado |= abs(nueva_distancia_picos - distancia_picos_actual /
                     umbral_polirritmico[i]) <= *umbral_minimo;
  }

  return resultado;
}