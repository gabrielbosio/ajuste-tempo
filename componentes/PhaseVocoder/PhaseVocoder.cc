// Copyright (c) 2013--2017, librosa development team.

#include <cmath>
#include "PhaseVocoder.h"

PhaseVocoder::PhaseVocoder(const float* tabla_senos, size_t longitud) :
  tabla_senos(tabla_senos),
  longitud_tabla(longitud) {}

void PhaseVocoder::Ejecutar(float* destino, float* origen,
                            size_t longitud_destino, size_t longitud_origen,
                            float ratio, size_t longitud_ventana,
                            size_t salto)
{
  float paso_ventana = longitud_ventana * ratio;

  for (int i = 0, j = 0; i < longitud_origen && j < longitud_destino;
       i += paso_ventana, j += longitud_ventana) {
    float paso = (float)i / longitud_ventana;
    int indice_ultimo_actual = (int)paso * longitud_ventana + 1;
    int indice_ultimo_proximo = indice_ultimo_actual + longitud_ventana;
    float ultimo_actual = origen[indice_ultimo_actual];
    float ultimo_proximo = 0.0F;
    if (i < longitud_origen - longitud_ventana) {
      ultimo_proximo = origen[indice_ultimo_proximo];
      origen[indice_ultimo_proximo] = 0.0F;
    }
    origen[indice_ultimo_actual] = 0.0F;
    ProcesarVentana(destino, origen, longitud_destino, longitud_origen, i,
                    j, paso, longitud_ventana, salto);
    float alfa = fmodf(paso, 1.0F);
    destino[j + 1] = (1.0F - alfa) * ultimo_actual + alfa * ultimo_proximo;
    origen[indice_ultimo_actual] = ultimo_actual;
    if (i < longitud_origen - longitud_ventana) {
      origen[indice_ultimo_proximo] = ultimo_proximo;
    }
  } 
}

void PhaseVocoder::ProcesarVentana(float* destino, float* origen,
                                   size_t longitud_destino,
                                   size_t longitud_origen, int i, int j,
                                   float paso, size_t longitud_ventana,
                                   size_t salto)
{
  for (int k = 0; k < longitud_ventana / 2; k++) {
    int ventana_actual_origen = k * 2 + (int)paso * longitud_ventana;
    int ventana_actual_destino = k * 2 + j;
    int ventana_proxima_origen = ventana_actual_origen + longitud_ventana;
    float inicio_paso_re = origen[ventana_actual_origen];
    float inicio_paso_im = origen[ventana_actual_origen + 1];
    float inicio_paso_mag = hypotf(inicio_paso_im, inicio_paso_re);
    float inicio_paso_ang = atan2f(inicio_paso_im, inicio_paso_re);
    float fin_paso_mag = 0.0F;
    float fin_paso_ang = 0.0F;
    if (i < longitud_origen - longitud_ventana) {
      float fin_paso_re = origen[ventana_proxima_origen];
      float fin_paso_im = origen[ventana_proxima_origen + 1];
      fin_paso_mag = hypotf(fin_paso_im, fin_paso_re);
      fin_paso_ang = atan2f(fin_paso_im, fin_paso_re);
    }
    float alfa = fmodf(paso, 1.0F);
    float mag = (1.0F - alfa) * inicio_paso_mag + alfa * fin_paso_mag;
    float avance_fase = 0.0F;
    if (longitud_ventana > 2) {
      avance_fase = M_PI * salto * ((float)k / (longitud_ventana - 2));
    }
    float ang_inicial = 0.0F;
    if (k > 0) {
      ang_inicial = atan2f(origen[k * 2 + 1], origen[k * 2]);
    }
    float ang = destino[ventana_actual_destino] + ang_inicial;
    float dos_pi = 2.0F * M_PI;
    if (j < longitud_destino - longitud_ventana) {
      float diferencia_fase = destino[ventana_actual_destino] +
                              fin_paso_ang - inicio_paso_ang - avance_fase;
      diferencia_fase -= dos_pi * roundf(diferencia_fase / dos_pi);
      float fase_acumulada = diferencia_fase + avance_fase;
      destino[ventana_actual_destino + longitud_ventana] = fase_acumulada;
    }
    float dos_pi_sobre_n = dos_pi / (longitud_tabla / 2);
    float arg = ang / dos_pi_sobre_n;
    int m = roundf(arg) * 2;
    m %= longitud_tabla;
    if (m < 0) {
      m = longitud_tabla - m;
    }
    destino[ventana_actual_destino] = mag * tabla_senos[m];
    destino[ventana_actual_destino + 1] = mag * tabla_senos[m + 1];
  }
}