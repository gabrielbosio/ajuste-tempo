// Copyright (c) 2013--2017, librosa development team.

#ifndef PHASE_VOCODER_H
#define PHASE_VOCODER_H

#include <cstddef>

/**
 * Calculador de Phase Vocoder
*/
class PhaseVocoder {
public:
  /**
   * Crea un calculador de Phase Vocoder.
   * Phase Vocoder modifica un espectro de forma tal que su representacion 
   * temporal resulta escalada en tiempo.
   * @param tabla_senos Array de valores precalculados de las funciones seno 
   * y coseno. Este gestor supone tabla_senos[i * 2] = cosf(i * N) y 
   * tabla_senos[i * 2 + 1] = sinf(i * N), donde 
   * N = 2 * M_PI / (longitud_tabla / 2).
   * @param longitud_tabla Longitud de tabla_senos.
  */
  PhaseVocoder(const float* tabla_senos, size_t longitud);
  /**
   * Dada origen, un array de espectros de ventana, escala en tiempo cada 
   * ventana en una proporcion dada por ratio y almacena en destino los 
   * espectros de las ventanas escaladas. 
   * Por ejemplo, si ratio == 2, este calculador escala en tiempo la 
   * cada ventana de origen al doble de su longitud y almacena en destino 
   * los espectros de las ventanas escaladas.
   * @param destino Array donde se van a almacenar los espectros de las 
   * ventanas escaladas.
   * @param origen Array de espectros de ventanas a escalar.
   * @param longitud_destino Longitud de destino. Dada longitud_audio la 
   * longitud del array de ventanas unidas en una unica pista de audio, se 
   * debe cumplir: 
   * longitud_destino == 
   * longitud_ventana * ((longitud_audio / ratio) / salto + 1)
   * @param longitud_origen Longitud de origen. Dada longitud_audio la 
   * longitud del array de ventanas unidas en una unica pista de audio, se 
   * debe cumplir: 
   * longitud_origen == longitud_ventana * (longitud_audio / salto + 1)
   * @param ratio Proporcion de escala en tiempo.
   * @param longitud_ventana Longitud de cada ventana de origen.
   * @param salto Cantidad de muestras que toma cada ventana en llegar a su 
   * maximo.
  */
  void Ejecutar(float* destino, float* origen, size_t longitud_destino,
                size_t longitud_origen, float ratio, size_t longitud_ventana,
                size_t salto);
private:
  const float* tabla_senos;
  size_t longitud_tabla;
  void ProcesarVentana(float* destino, float* origen, size_t longitud_destino,
                       size_t longitud_origen, int i, int j, float paso,
                       size_t longitud_ventana, size_t salto);
};

#endif