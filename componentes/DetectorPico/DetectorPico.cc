#include "DetectorPico.h"

DetectorPico::DetectorPico(float intensidad_pico, int* picos,
                           size_t cantidad_maxima_picos) :
  intensidad_pico(intensidad_pico),
  picos(picos),
  cantidad_maxima_picos(cantidad_maxima_picos) {}

bool DetectorPico::Ejecutar(float muestra, int instante)
{
  cantidad_picos_ %= cantidad_maxima_picos;
  if (muestra > intensidad_pico) {
    if (!ultima_muestra_fue_pico) {
      picos[cantidad_picos_++] = instante;
      ultima_muestra_fue_pico = true;
    }
  } else {
    ultima_muestra_fue_pico = false;
  }
  if (cantidad_picos_ == cantidad_maxima_picos) {
    return true;
  }
  return false;
}

size_t DetectorPico::cantidad_picos() { return cantidad_picos_; }