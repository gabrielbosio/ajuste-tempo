#ifndef DETECTOR_PICO_H
#define DETECTOR_PICO_H

#include <cstddef>

/**
 * Detector y almacenador de picos de intensidad.
*/
class DetectorPico {
public:
  /**
   * Crea un detector de picos con un valor de intensidad minimo para 
   * establecer si una muestra es un pico.
   * @param intensidad_pico Intensidad minima de una muestra para ser 
   * considerada un pico.
   * @param picos Buffer en donde se almacenan los picos detectados.
   * @param cantidad_maxima_picos Longitud del buffer.
  */
  DetectorPico(float intensidad_pico, int* picos,
               size_t cantidad_maxima_picos);
  /**
   * Decide si muestra es un pico. Si lo es, almacena instante en el buffer 
   * de picos pasado como argumento en el constructor. Si el buffer esta 
   * lleno, vacia el buffer antes de almacenar instante.
   * @param muestra Intensidad de la muestra a decidir si es un pico.
   * @param instante Instante de tiempo, en milisegundos, asociado a la 
   * muestra.
   * @return true si el buffer de picos esta lleno. De lo contrario, false.
  */
  bool Ejecutar(float muestra, int instante);
  /**
   * Cantidad de picos almacenados en el buffer de picos pasado como 
   * argumento en el constructor.
  */
  size_t cantidad_picos();
private:
  float intensidad_pico;
  int* picos;
  size_t cantidad_maxima_picos;
  size_t cantidad_picos_ = 0;
  bool ultima_muestra_fue_pico = false;
};

#endif