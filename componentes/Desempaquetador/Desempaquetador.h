#ifndef DESEMPAQUETADOR_H
#define DESEMPAQUETADOR_H

#include <cstddef>
#include <cstdint>

/**
 * Estructura resultante de desempaquetar una trama de datos.
*/
struct Configuracion {
  float tempo;
  int umbral_minimo;
  uint8_t* pista;
  static const size_t longitud_umbral = 4;
  int umbral_polirritmico[longitud_umbral];
};

/**
 * Desempaquetador de tramas de datos.
*/
class Desempaquetador {
public:
  /**
   * Dada una trama de datos buffer, divide la trama en partes y establece 
   * configuracion siguiendo el formato: 
   * [t0, t1, t2, u_m, u_p, p0, p1, ..., pk], 
   * donde: 
   * t0, t1, t2 son digitos, 
   * tempo = t0 * 100 + t1 * 10 + t2, 
   * umbral_minimo = u_m, 
   * umbral_polirritmico = u_p, 
   * pista = [p0, p1, ..., pk]. 
   * La trama de datos puede excluir los bytes p0, p1, ..., pk. En ese caso, 
   * este desempaquetador no establece la pista en configuracion.
   * @param buffer Trama de datos a dividir.
   * @param longitud Longitud de buffer.
   * @param configuracion Estructura donde se va a guardar la informacion 
   * tomada de buffer.
   * @return true si buffer sigue el formato. De lo contrario, false.
  */
  bool Desempaquetar(uint8_t* buffer, size_t longitud,
                     Configuracion* configuracion);
private:
  static const size_t longitud_tempo = 3;
  static const size_t longitud_tempo_y_umbral = 4;
  static const size_t longitud_configuracion = 5;
};

#endif