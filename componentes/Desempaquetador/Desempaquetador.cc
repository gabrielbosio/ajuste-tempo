#include "Desempaquetador.h"

bool Desempaquetador::Desempaquetar(uint8_t* buffer, size_t longitud,
                                    Configuracion* configuracion)
{
  if (longitud < longitud_configuracion || buffer[0] > 9 || buffer[1] > 9 ||
      buffer[2] > 9) {
    return false;
  }

  if (longitud > longitud_tempo) {
    configuracion->umbral_minimo = buffer[3];
  }
  if (configuracion->pista != nullptr && longitud > longitud_configuracion) {
    configuracion->pista = buffer + longitud_configuracion;
  }
  configuracion->tempo = buffer[0] * 100 + buffer[1] * 10 + buffer[2];
  configuracion->umbral_polirritmico[0] = 2;
  configuracion->umbral_polirritmico[1] = 0;
  configuracion->umbral_polirritmico[2] = 4;
  configuracion->umbral_polirritmico[3] = 0;
  if (longitud > longitud_tempo_y_umbral && buffer[4] == 1) {
    configuracion->umbral_polirritmico[1] = 3;
    configuracion->umbral_polirritmico[3] = 6;
  }
  
  return true;
}