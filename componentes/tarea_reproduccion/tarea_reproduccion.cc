#include <cstdint>
#include "LectorAudio.h"
#include "ImplCodecI2S.h"
#include "ReproductorAudio.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "tarea_reproduccion.h"

void tarea_reproduccion(void* parametros)
{
  auto lector = (LectorAudio*)parametros;
  auto i2s = new ImplCodecI2S();
  auto reproductor = new ReproductorAudio(i2s);

  while (true) {
    lector->Leer();
    reproductor->Reproducir(lector->buffer_salida(),
                            lector->longitud_contenido_salida());
    vTaskDelay(pdMS_TO_TICKS(100));
  }

  delete i2s;
  delete reproductor;
  vTaskDelete(nullptr);
}