#ifndef TAREA_REPRODUCCION_H
#define TAREA_REPRODUCCION_H

/**
 * Tarea de reproduccion de la pista de audio. 
 * Para crear esta tarea, llamar a la funcion xTaskCreate de la biblioteca 
 * FreeRTOS con esta funcion como argumento.
 * @param parametros El parametro que recibe esta tarea debe ser un objeto 
 * LectorAudio.
*/
void tarea_reproduccion(void* parametros);

#endif