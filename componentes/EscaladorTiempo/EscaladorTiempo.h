#ifndef ESCALADOR_TIEMPO_H
#define ESCALADOR_TIEMPO_H

#include <cstddef>
#include <cstdint>
#include "GestorVentanas.h"
#include "TrFourier.h"
#include "PhaseVocoder.h"

/**
 * Escalador en tiempo en base a la tecnica de Phase Vocoder.
*/
class EscaladorTiempo {
public:
  /**
   * Crea un escalador en tiempo.
   * Reserva memoria para almacenar los resultados de la STFT y de Phase 
   * Vocoder al llamar al metodo Ejecutar().
   * @param longitud_maxima Longitud maxima del array donde se va a almacenar 
   * el resultado de la escala en tiempo.
   * @param gestor_ventanas Junto con fft, conforman la ejecucion de la STFT.
   * @param fft Encargado de transformar un array de muestras en tiempo a un 
   * array de muestras en frecuencia.
   * @param phase_vocoder Encargado de modificar la fase de un array de 
   * muestras en frecuencia. Esto se refleja en tiempo como una escala.
  */
  EscaladorTiempo(size_t longitud_maxima, GestorVentanas* gestor_ventanas,
                  TrFourier* fft, PhaseVocoder* phase_vocoder);
  /**
   * Libera la memoria reservada para almacenar los resultados de la STFT y 
   * de Phase Vocoder al llamar al metodo Ejecutar().
  */
  ~EscaladorTiempo();
  /**
   * Dado un array de muestras temporales origen, escala en tiempo ese array 
   * y almacena el resultado en destino. 
   * La proporcion de la escala es: longitud_destino / longitud_origen. 
   * Por ejemplo, si longitud_destino == longitud_origen * 2, el array de 
   * muestras se va a escalar en tiempo al doble de su longitud. 
   * La escala en tiempo consiste en el calculo de la STFT de origen, seguido 
   * de la ejecucion de Phase Vocoder en el resultado de la STFT, y el 
   * calculo de la STFT inversa del resultado de Phase Vocoder.
   * @param destino Array donde se va a almacenar el resultado de la escala 
   * en tiempo.
   * @param origen Array de muestras escalar en tiempo.
   * @param longitud_destino Longitud de destino.
   * @param longitud_origen Longitud de origen.
  */
  void Ejecutar(float* destino, uint8_t* origen, size_t longitud_destino,
                size_t longitud_origen);
private:
  size_t longitud_maxima;
  const size_t longitud_maxima_stft;
  float* matriz_stft;
  float* matriz_stft_escalada;
  GestorVentanas* gestor_ventanas;
  TrFourier* fft;
  PhaseVocoder* phase_vocoder;
  void AplicarSTFT(uint8_t* audio, size_t longitud_audio,
                   size_t* longitud_stft);
  void AplicarPhaseVocoder(size_t longitud_stft, size_t longitud_audio,
                           float ratio, size_t* longitud_escalada);
  void AplicarISTFT(float* audio, size_t longitud_audio, size_t longitud_stft,
                    float ratio);
};

#endif