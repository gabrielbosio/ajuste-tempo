// Copyright (c) 2013--2017, librosa development team.
#include <cmath>
#include <cstring>
#include <iostream>
#include "EscaladorTiempo.h"

EscaladorTiempo::EscaladorTiempo(size_t longitud_maxima,
                                 GestorVentanas* gestor_ventanas,
                                 TrFourier* fft,
                                 PhaseVocoder* phase_vocoder) :
  longitud_maxima(longitud_maxima),
  longitud_maxima_stft(gestor_ventanas->longitud_ventana() *
                       (longitud_maxima / gestor_ventanas->salto() + 1)),
  matriz_stft(new float[longitud_maxima_stft]),
  matriz_stft_escalada(new float[longitud_maxima_stft]),
  gestor_ventanas(gestor_ventanas),
  fft(fft),
  phase_vocoder(phase_vocoder)
{
  std::memset(matriz_stft_escalada, 0, gestor_ventanas->longitud_ventana() *
              sizeof(float));
}

void EscaladorTiempo::Ejecutar(float* destino, uint8_t* origen,
                               size_t longitud_destino,
                               size_t longitud_origen)
{
  longitud_origen = fminf(longitud_origen, longitud_maxima);
  longitud_destino = fminf(longitud_destino, longitud_maxima);

  size_t longitud_stft;
  AplicarSTFT(origen, longitud_origen, &longitud_stft);

  float ratio = (float)longitud_origen / longitud_destino;
  size_t longitud_escalada;
  AplicarPhaseVocoder(longitud_stft, longitud_origen, ratio,
                      &longitud_escalada);

  AplicarISTFT(destino, longitud_destino, longitud_escalada, ratio);
}

EscaladorTiempo::~EscaladorTiempo()
{
  delete[] matriz_stft;
  delete[] matriz_stft_escalada;
}

void EscaladorTiempo::AplicarSTFT(uint8_t* audio, size_t longitud_audio,
                                  size_t* longitud_stft)
{
  size_t longitud_ventana = gestor_ventanas->longitud_ventana();
  size_t salto = gestor_ventanas->salto();
  if (longitud_ventana > longitud_audio) {
    std::cout << "La longitud de la ventana (" << longitud_ventana << ")";
    std::cout << " es mayor a la longitud de la entrada (" << longitud_audio;
    std::cout << ")\n";
  }

  size_t cantidad_ventanas = longitud_audio / salto + 1;
  *longitud_stft = fminf(longitud_ventana * cantidad_ventanas,
                         longitud_maxima_stft);
  auto ventana_audio = new float[longitud_ventana];
  fft->direccion(DireccionFFT::adelante);
  fft->entrada(ventana_audio);
  
  for (int i = 0; i < cantidad_ventanas; i++) {
    gestor_ventanas->AplicarVentana(ventana_audio, audio, longitud_audio, i);
    fft->salida(matriz_stft + i * longitud_ventana);
    fft->Ejecutar();
  }

  delete[] ventana_audio;
}

void EscaladorTiempo::AplicarPhaseVocoder(size_t longitud_stft,
                                          size_t longitud_audio, float ratio,
                                          size_t* longitud_escalada)
{
  size_t longitud_ventana = gestor_ventanas->longitud_ventana();
  size_t salto = gestor_ventanas->salto();
  int nueva_cantidad_ventanas = (longitud_audio / ratio) / salto + 1;
  *longitud_escalada = fminf(longitud_ventana * nueva_cantidad_ventanas,
                             longitud_maxima_stft);
  phase_vocoder->Ejecutar(matriz_stft_escalada, matriz_stft,
                          *longitud_escalada, longitud_stft, ratio,
                          longitud_ventana, salto);
}

void EscaladorTiempo::AplicarISTFT(float* audio, size_t longitud_audio,
                                   size_t longitud_stft, float ratio)
{
  size_t longitud_ventana = gestor_ventanas->longitud_ventana();
  int cantidad_ventanas = longitud_stft / longitud_ventana;
  auto ventana_audio = new float[longitud_ventana];
  fft->direccion(DireccionFFT::atras);
  fft->salida(ventana_audio);

  for (int i = 0; i < cantidad_ventanas; i++) {
    fft->entrada(matriz_stft_escalada + i * longitud_ventana);
    fft->Ejecutar();
    gestor_ventanas->UnirAudio(audio, ventana_audio, longitud_audio, i);
  }

  gestor_ventanas->Normalizar(audio, longitud_audio);
  
  delete[] ventana_audio;
}