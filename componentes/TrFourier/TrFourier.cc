/*

  ESP32 FFT
  =========

  This provides a vanilla radix-2 FFT implementation and a test example.

  Author
  ------

  This code was written by [Robin Scheibler](http://www.robinscheibler.org) during rainy days in October 2017.

  License
  -------

  Copyright (c) 2017 Robin Scheibler

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/
#include <cmath>

#include "TrFourier.h"

TrFourier::TrFourier(size_t longitud_espectro, const float* tabla_senos,
                     size_t longitud_tabla, TipoFFT tipo) :
  longitud_espectro(longitud_espectro),
  tabla_senos(tabla_senos),
  longitud_tabla(longitud_tabla),
  tipo(tipo) {}

void TrFourier::Ejecutar()
{
  if (tipo == TipoFFT::real && direccion_ == DireccionFFT::adelante) {
    rfft();
  } else if (tipo == TipoFFT::real && direccion_ == DireccionFFT::atras) {
    irfft();
  } else if (tipo == TipoFFT::complejo &&
             direccion_ == DireccionFFT::adelante) {
    fft();
  } else if (tipo == TipoFFT::complejo && direccion_ == DireccionFFT::atras) {
    ifft();
  }
}

float* TrFourier::entrada() { return entrada_; }

void TrFourier::entrada(float* entrada)
{
  entrada_ = entrada;
}

float* TrFourier::salida() { return salida_; }

void TrFourier::salida(float* salida)
{
  salida_ = salida;
}

DireccionFFT TrFourier::direccion() { return direccion_; }

void TrFourier::direccion(DireccionFFT nueva_direccion)
{
  direccion_ = nueva_direccion;
}

void TrFourier::fft()
{
  /*
   * Forward fast Fourier transform
   * DIT, radix-2, out-of-place implementation
   *
   * Parameters
   * ----------
   *  input (float *)
   *    The input array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  output (float *)
   *    The output array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  n (int)
   *    The FFT size, should be a power of 2
   */
  if (use_split_radix) {
    split_radix_fft(entrada_, salida_, longitud_espectro, 2, 2);
  } else {
    fft_primitive(entrada_, salida_, longitud_espectro, 2, 2);
  }
}

void TrFourier::ifft()
{
  /*
   * Inverse fast Fourier transform
   * DIT, radix-2, out-of-place implementation
   *
   * Parameters
   * ----------
   *  input (float *)
   *    The input array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  output (float *)
   *    The output array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  n (int)
   *    The FFT size, should be a power of 2
   */
  ifft_primitive(entrada_, salida_, longitud_espectro, 2, 2);
}

void TrFourier::rfft()
{
  int salto_tabla = (longitud_tabla / 2) / longitud_espectro;
  // This code uses the two-for-the-price-of-one strategy
  if (use_split_radix) {
    split_radix_fft(entrada_, salida_, longitud_espectro / 2, 2,
                    salto_tabla * 4);
  } else {
    fft_primitive(entrada_, salida_, longitud_espectro / 2, 2,
                  salto_tabla * 4);
  }

  // Now apply post processing to recover positive
  // frequencies of the real FFT
  float t = salida_[0];
  salida_[0] = t + salida_[1];  // DC coefficient
  salida_[1] = t - salida_[1];  // Center coefficient

  // Apply post processing to quarter element
  // this boils down to taking complex conjugate
  salida_[longitud_espectro / 2 + 1] = -salida_[longitud_espectro / 2 + 1];

  // Now process all the other frequencies
  int k;
  for (k = 2; k < longitud_espectro / 2; k += 2) {
    float xer, xei, xOr, xoi, c, s, tr, ti;

    c = tabla_senos[k * salto_tabla];
    s = tabla_senos[k * salto_tabla + 1];
    
    // even half coefficient
    xer = 0.5 * (salida_[k] + salida_[longitud_espectro - k]);
    xei = 0.5 * (salida_[k + 1] - salida_[longitud_espectro - k + 1]);

    // odd half coefficient
    xOr = 0.5 * (salida_[k + 1] + salida_[longitud_espectro - k + 1]);
    xoi = -0.5 * (salida_[k] - salida_[longitud_espectro - k]);

    tr = c * xOr + s * xoi;
    ti = -s * xOr + c * xoi;

    salida_[k] = xer + tr;
    salida_[k + 1] = xei + ti;

    salida_[longitud_espectro - k] = xer - tr;
    salida_[longitud_espectro - k + 1] = -(xei - ti);
  }
}

void TrFourier::irfft()
{
  /*
   * Destroys content of input vector
   */
  int salto_tabla = (longitud_tabla / 2) / longitud_espectro;

  // Here we need to apply a pre-processing first
  float t = entrada_[0];
  entrada_[0] = 0.5 * (t + entrada_[1]);
  entrada_[1] = 0.5 * (t - entrada_[1]);

  entrada_[longitud_espectro / 2 + 1] = -entrada_[longitud_espectro / 2 + 1];

  int k;
  for (k = 2; k < longitud_espectro / 2; k += 2) {
    float xer, xei, xOr, xoi, c, s, tr, ti;

    c = tabla_senos[k * salto_tabla];
    s = tabla_senos[k * salto_tabla + 1];

    xer = 0.5 * (entrada_[k] + entrada_[longitud_espectro - k]);
    tr  = 0.5 * (entrada_[k] - entrada_[longitud_espectro - k]);

    xei = 0.5 * (entrada_[k + 1] - entrada_[longitud_espectro - k + 1]);
    ti  = 0.5 * (entrada_[k + 1] + entrada_[longitud_espectro - k + 1]);

    xOr = c * tr - s * ti;
    xoi = s * tr + c * ti;

    entrada_[k] = xer - xoi;
    entrada_[k + 1] = xOr + xei;

    entrada_[longitud_espectro - k] = xer + xoi;
    entrada_[longitud_espectro - k + 1] = xOr - xei;
  }

  ifft_primitive(entrada_, salida_, longitud_espectro / 2, 2,
                 salto_tabla * 4);
}

void TrFourier::fft_primitive(float* x, float* y, int n, int salto,
                              int salto_tabla)
{
  /*
   * This code will compute the FFT of the input vector x
   *
   * The input data is assumed to be real/imag interleaved
   *
   * The size n should be a power of two
   *
   * y is an output buffer of size 2n to accomodate for complex numbers
   *
   * Forward fast Fourier transform
   * DIT, radix-2, out-of-place implementation
   *
   * For a complex FFT, call first stage as:
   * fft(x, y, n, 2, 2);
   *
   * Parameters
   * ----------
   *  x (float *)
   *    The input array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  y (float *)
   *    The output array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  n (int)
   *    The FFT size, should be a power of 2
   *  stride (int)
   *    The number of elements to skip between two successive samples
   *  tw_stride (int)
   *    The number of elements to skip between two successive twiddle factors
   */
  int k;
  float t;

  if (large_base_case) {
    // End condition, stop at n=8 to avoid one trivial recursion
    if (n == 8) {
      fft8(x, salto, y, 2);
      return;
    }
  } else {
    // End condition, stop at n=2 to avoid one trivial recursion
    if (n == 2) {
      y[0] = x[0] + x[salto];
      y[1] = x[1] + x[salto + 1];
      y[2] = x[0] - x[salto];
      y[3] = x[1] - x[salto + 1];
      return;
    }
  }

  // Recursion -- Decimation In Time algorithm
  // even half
  fft_primitive(x, y, n / 2, 2 * salto, 2 * salto_tabla);
  // odd half
  fft_primitive(x + salto, y + n, n / 2, 2 * salto, 2 * salto_tabla);

  // Stitch back together

  // We can a few multiplications in the first step
  t = y[0];
  y[0] = t + y[n];
  y[n] = t - y[n];

  t = y[1];
  y[1] = t + y[n + 1];
  y[n + 1] = t - y[n + 1];

  for (k = 1; k < n / 2; k++) {
    float x1r, x1i, x2r, x2i, c, s;
    c = tabla_senos[k * salto_tabla];
    s = tabla_senos[k * salto_tabla + 1];

    x1r = y[2 * k];
    x1i = y[2 * k + 1];
    x2r =  c * y[n + 2 * k] + s * y[n + 2 * k + 1];
    x2i = -s * y[n + 2 * k] + c * y[n + 2 * k + 1];

    y[2 * k] = x1r + x2r;
    y[2 * k + 1] = x1i + x2i;

    y[n + 2 * k] = x1r - x2r;
    y[n + 2 * k + 1] = x1i - x2i;
  }

}

void TrFourier::split_radix_fft(float* x, float* y, int n, int salto,
                                int salto_tabla)
{
  /*
   * This code will compute the FFT of the input vector x
   *
   * The input data is assumed to be real/imag interleaved
   *
   * The size n should be a power of two
   *
   * y is an output buffer of size 2n to accomodate for complex numbers
   *
   * Forward fast Fourier transform
   * Split-Radix
   * DIT, radix-2, out-of-place implementation
   *
   * For a complex FFT, call first stage as:
   * fft(x, y, n, 2, 2);
   *
   * Parameters
   * ----------
   *  x (float *)
   *    The input array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  y (float *)
   *    The output array containing the complex samples with
   *    real/imaginary parts interleaved [Re(x0), Im(x0), ..., Re(x_n-1), Im(x_n-1)]
   *  n (int)
   *    The FFT size, should be a power of 2
   *  stride (int)
   *    The number of elements to skip between two successive samples
   *  twiddle_factors (float *)
   *    The array of twiddle factors
   *  tw_stride (int)
   *    The number of elements to skip between two successive twiddle factors
   */
  int k;

  if (large_base_case) {
    // End condition, stop at n=2 to avoid one trivial recursion
    if (n == 8) {
      fft8(x, salto, y, 2);
      return;
    } else if (n == 4) {
      fft4(x, salto, y, 2);
      return;
    }
  } else {
    // End condition, stop at n=2 to avoid one trivial recursion
    if (n == 2) {
      y[0] = x[0] + x[salto];
      y[1] = x[1] + x[salto + 1];
      y[2] = x[0] - x[salto];
      y[3] = x[1] - x[salto + 1];
      return;
    } else if (n == 1) {
      y[0] = x[0];
      y[1] = x[1];
      return;
    }
  }

  // Recursion -- Decimation In Time algorithm
  split_radix_fft(x, y, n / 2, 2 * salto, 2 * salto_tabla);
  split_radix_fft(x + salto, y + n, n / 4, 4 * salto, 4 * salto_tabla);
  split_radix_fft(x + 3 * salto, y + n + n / 2, n / 4, 4 * salto,
                  4 * salto_tabla);

  // Stitch together the output
  float u1r, u1i, u2r, u2i, x1r, x1i, x2r, x2i;
  float t;

  // We can save a few multiplications in the first step
  u1r = y[0];
  u1i = y[1];
  u2r = y[n / 2];
  u2i = y[n / 2 + 1];

  x1r = y[n];
  x1i = y[n + 1];
  x2r = y[n / 2 + n];
  x2i = y[n / 2 + n + 1];

  t = x1r + x2r;
  y[0] = u1r + t;
  y[n] = u1r - t;

  t = x1i + x2i;
  y[1] = u1i + t;
  y[n + 1] = u1i - t;

  t = x2i - x1i;
  y[n / 2] = u2r - t;
  y[n + n / 2] = u2r + t;

  t = x1r - x2r;
  y[n / 2 + 1] = u2i - t;
  y[n + n / 2 + 1] = u2i + t;

  for (k = 1; k < n / 4; k++) {
    float u1r, u1i, u2r, u2i, x1r, x1i, x2r, x2i, c1, s1, c2, s2;
    c1 = tabla_senos[k * salto_tabla];
    s1 = tabla_senos[k * salto_tabla + 1];
    c2 = tabla_senos[3 * k * salto_tabla];
    s2 = tabla_senos[3 * k * salto_tabla + 1];

    u1r = y[2 * k];
    u1i = y[2 * k + 1];
    u2r = y[2 * k + n / 2];
    u2i = y[2 * k + n / 2 + 1];

    x1r =  c1 * y[n + 2 * k] + s1 * y[n + 2 * k + 1];
    x1i = -s1 * y[n + 2 * k] + c1 * y[n + 2 * k + 1];
    x2r =  c2 * y[n / 2 + n + 2 * k] + s2 * y[n / 2 + n + 2 * k + 1];
    x2i = -s2 * y[n / 2 + n + 2 * k] + c2 * y[n / 2 + n + 2 * k + 1];

    t = x1r + x2r;
    y[2 * k] = u1r + t;
    y[2 * k + n] = u1r - t;

    t = x1i + x2i;
    y[2 * k + 1] = u1i + t;
    y[2 * k + n + 1] = u1i - t;

    t = x2i - x1i;
    y[2 * k + n / 2] = u2r - t;
    y[2 * k + n + n / 2] = u2r + t;

    t = x1r - x2r;
    y[2 * k + n / 2 + 1] = u2i - t;
    y[2 * k + n + n / 2 + 1] = u2i + t;
  }
}

void TrFourier::ifft_primitive(float* entrada, float* salida, int n,
                               int salto, int salto_tabla)
{
  if (use_split_radix) {
    split_radix_fft(entrada, salida, n, salto, salto_tabla);
  } else {
    fft_primitive(entrada, salida, n, salto, salto_tabla);
  }

  int ks;
  int ns = n * salto;

  // Convertir los coeficientes de 1 a n / 2 - 1
  for (ks = salto; ks < ns / 2; ks += salto) {
    float t;

    t = salida[ks];
    salida[ks] = salida[ns - ks];
    salida[ns - ks] = t;

    t = salida[ks + 1];
    salida[ks + 1] = salida[ns - ks + 1];
    salida[ns - ks + 1] = t;
  }

  // Normalizar
  float norm = 1.0F / n;
  for (ks = 0; ks < ns; ks += salto) {
    salida[ks] *= norm;
    salida[ks + 1] *= norm;
  }
}

inline void TrFourier::fft8(float* entrada, int salto_entrada, float* salida,
                            int salto_salida)
{
  /*
   * Unrolled implementation of FFT8 for a little more performance
   */
  float a0r, a1r, a2r, a3r, a4r, a5r, a6r, a7r;
  float a0i, a1i, a2i, a3i, a4i, a5i, a6i, a7i;
  float b0r, b1r, b2r, b3r, b4r, b5r, b6r, b7r;
  float b0i, b1i, b2i, b3i, b4i, b5i, b6i, b7i;
  float t;
  float sin_pi_4 = 0.7071067812;

  a0r = entrada[0];
  a0i = entrada[1];
  a1r = entrada[salto_entrada];
  a1i = entrada[salto_entrada + 1];
  a2r = entrada[2 * salto_entrada];
  a2i = entrada[2 * salto_entrada + 1];
  a3r = entrada[3 * salto_entrada];
  a3i = entrada[3 * salto_entrada + 1];
  a4r = entrada[4 * salto_entrada];
  a4i = entrada[4 * salto_entrada + 1];
  a5r = entrada[5 * salto_entrada];
  a5i = entrada[5 * salto_entrada + 1];
  a6r = entrada[6 * salto_entrada];
  a6i = entrada[6 * salto_entrada + 1];
  a7r = entrada[7 * salto_entrada];
  a7i = entrada[7 * salto_entrada + 1];

  // Stage 1

  b0r = a0r + a4r;
  b0i = a0i + a4i;

  b1r = a1r + a5r;
  b1i = a1i + a5i;

  b2r = a2r + a6r;
  b2i = a2i + a6i;

  b3r = a3r + a7r;
  b3i = a3i + a7i;

  b4r = a0r - a4r;
  b4i = a0i - a4i;

  b5r = a1r - a5r;
  b5i = a1i - a5i;
  // W_8^1 = 1/sqrt(2) - j / sqrt(2)
  t = b5r + b5i;
  b5i = (b5i - b5r) * sin_pi_4;
  b5r = t * sin_pi_4;

  // W_8^2 = -j
  b6r = a2i - a6i;
  b6i = a6r - a2r;

  b7r = a3r - a7r;
  b7i = a3i - a7i;
  // W_8^3 = -1 / sqrt(2) + j / sqrt(2)
  t = sin_pi_4 * (b7i - b7r);
  b7i = - (b7r + b7i) * sin_pi_4;
  b7r = t;

  // Stage 2

  a0r = b0r + b2r;
  a0i = b0i + b2i;

  a1r = b1r + b3r;
  a1i = b1i + b3i;

  a2r = b0r - b2r;
  a2i = b0i - b2i;

  // * j
  a3r = b1i - b3i;
  a3i = b3r - b1r;

  a4r = b4r + b6r;
  a4i = b4i + b6i;

  a5r = b5r + b7r;
  a5i = b5i + b7i;

  a6r = b4r - b6r;
  a6i = b4i - b6i;

  // * j
  a7r = b5i - b7i;
  a7i = b7r - b5r;

  // Stage 3

  // X[0]
  salida[0] = a0r + a1r;
  salida[1] = a0i + a1i;

  // X[4]
  salida[4 * salto_salida] = a0r - a1r;
  salida[4 * salto_salida + 1] = a0i - a1i;

  // X[2]
  salida[2 * salto_salida] = a2r + a3r;
  salida[2 * salto_salida + 1] = a2i + a3i;

  // X[6]
  salida[6 * salto_salida] = a2r - a3r;
  salida[6 * salto_salida + 1] = a2i - a3i;

  // X[1]
  salida[salto_salida] = a4r + a5r;
  salida[salto_salida + 1] = a4i + a5i;

  // X[5]
  salida[5 * salto_salida] = a4r - a5r;
  salida[5 * salto_salida + 1] = a4i - a5i;

  // X[3]
  salida[3 * salto_salida] = a6r + a7r;
  salida[3 * salto_salida + 1] = a6i + a7i;

  // X[7]
  salida[7 * salto_salida] = a6r - a7r;
  salida[7 * salto_salida + 1] = a6i - a7i;

}

inline void TrFourier::fft4(float* entrada, int salto_entrada, float* salida,
                            int salto_salida)
{
  /*
   * Unrolled implementation of FFT4 for a little more performance
   */
  float t1, t2;

  t1 = entrada[0] + entrada[2 * salto_entrada];
  t2 = entrada[salto_entrada] + entrada[3 * salto_entrada];
  salida[0] = t1 + t2;
  salida[2 * salto_salida] = t1 - t2;

  t1 = entrada[1] + entrada[2 * salto_entrada + 1];
  t2 = entrada[salto_entrada + 1] + entrada[3 * salto_entrada + 1];
  salida[1] = t1 + t2;
  salida[2 * salto_salida + 1] = t1 - t2;

  t1 = entrada[0] - entrada[2 * salto_entrada];
  t2 = entrada[salto_entrada + 1] - entrada[3 * salto_entrada + 1];
  salida[salto_salida] = t1 + t2;
  salida[3 * salto_salida] = t1 - t2;

  t1 = entrada[1] - entrada[2 * salto_entrada + 1];
  t2 = entrada[3 * salto_entrada] - entrada[salto_entrada];
  salida[salto_salida + 1] = t1 + t2;
  salida[3 * salto_salida + 1] = t1 - t2;
}
