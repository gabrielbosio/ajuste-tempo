/*

  ESP32 FFT
  =========

  This provides a vanilla radix-2 FFT implementation and a test example.

  Author
  ------

  This code was written by [Robin Scheibler](http://www.robinscheibler.org) during rainy days in October 2017.

  License
  -------

  Copyright (c) 2017 Robin Scheibler

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/
#ifndef TR_FOURIER_H
#define TR_FOURIER_H

#include <cstddef>

/**
 * Tipo de dato de entrada de la Transformada de Fourier
*/
enum class TipoFFT {
  real, complejo
};

/**
 * Direccion de la Transformada de Fourier.
*/
enum class DireccionFFT {
  adelante, atras
};

/**
 * Calculador de la Transformada de Fourier.
*/
class TrFourier {
public:
  /**
   * Crea un calculador de la Transformada de Fourier. 
   * El algoritmo usado para calcular la Transformada de Fourier es la FFT. 
   * @param longitud_espectro Longitud de entrada() y salida()
   * @param tabla_senos Array de valores precalculados de las funciones seno 
   * y coseno. Este gestor supone tabla_senos[i * 2] = cosf(i * N) y 
   * tabla_senos[i * 2 + 1] = sinf(i * N), donde 
   * N = 2 * M_PI / (longitud_tabla / 2).
   * @param longitud_tabla Longitud de tabla_senos.
   * @param tipo Tipo de dato de entrada.
  */
  TrFourier(size_t longitud_espectro, const float* tabla_senos,
            size_t longitud_tabla, TipoFFT tipo);
  /**
   * Dado un array de entrada, ejecuta un algoritmo de FFT (real o complejo) 
   * en una direccion establecida y almacena el resultado en un array de 
   * salida. Antes de llamar a este metodo, establecer los arrays de entrada 
   * y salida llamando a los setters entrada() y salida().
   * Si la direccion establecida es DireccionFFT::adelante, este calculador 
   * ejecuta la FFT. Si la direccion establecida es DireccionFFT::atras, este 
   * calculador ejecuta la inversa de la FFT. Si no se establecio ninguna 
   * direccion, se toma el caso de DireccionFFT::adelante.
  */
  void Ejecutar();
  /**
   * Array de entrada usado para calcular la FFT.
  */
  float* entrada();
  /**
   * Establece el array de entrada usado para calcular la FFT.
  */
  void entrada(float* entrada);
  /**
   * Array de salida usado para almacenar el resultado de la FFT.
  */
  float* salida();
  /**
   * Establece el array de salida usado para almacenar el resultado de la FFT.
  */
  void salida(float* salida);
  /**
   * Direccion de la FFT. Define si, al llamar al metodo Ejecutar, este 
   * calculador ejecuta la FFT o su inversa.
  */
  DireccionFFT direccion();
  /**
   * Establece la direccion de la FFT. En las proximas llamadas al metodo 
   * Ejecutar, este calculador ejecutara la FFT o su inversa dependiendo de 
   * la nueva direccion establecida.
  */
  void direccion(DireccionFFT nueva_direccion);
private:
  size_t longitud_espectro;
  float* entrada_;
  float* salida_;
  const float* tabla_senos;
  size_t longitud_tabla;
  TipoFFT tipo;
  DireccionFFT direccion_ = DireccionFFT::adelante;
  static const bool use_split_radix = true;
  static const bool large_base_case = true;
  void fft();
  void ifft();
  void rfft();
  void irfft();
  void fft_primitive(float* x, float* y, int n, int salto, int tw_salto);
  void split_radix_fft(float* x, float* y, int n, int salto, int tw_salto);
  void ifft_primitive(float* entrada, float* salida, int n, int salto,
                      int tw_salto);
  void fft8(float* entrada, int salto_entrada, float* salida,
            int salto_salida);
  void fft4(float* entrada, int salto_entrada, float* salida,
            int salto_salida);
};

#endif // __FFT_H__
