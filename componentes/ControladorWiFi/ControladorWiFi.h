#ifndef CONTROLADOR_WI_FI_H
#define CONTROLADOR_WI_FI_H

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <esp_event_loop.h>

/**
 * Capa de abstraccion del modulo de Wi-Fi de ESP-IDF.
*/
class ControladorWiFi {
public:
  /**
   * Activa la funcionalidad de Wi-Fi. 
   * Establece el dispositivo en modo AP (punto de acceso). 
   * Otras estaciones podran conectarse a este dispositivo.
   * @param ssid Nombre del punto de acceso.
   * @param password Password del punto de acceso.
  */
  ControladorWiFi(const char* ssid, const char* password);
  /**
   * Desactiva la funcionalidad de Wi-Fi.
  */
  ~ControladorWiFi();
private:
  static const int canal = 1;
  static const int conexiones_maximas = 4;
  EventGroupHandle_t s_wifi_event_group;
  void InicializarNVS();
  static esp_err_t ciclo_ejecucion_wifi(void *ctx, system_event_t *event);
};

#endif