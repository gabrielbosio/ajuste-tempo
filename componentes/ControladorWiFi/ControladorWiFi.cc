#include <cstdio>
#include <cstring>
#include <esp_wifi.h>
#include "nvs_flash.h"
#include "ControladorWiFi.h"

ControladorWiFi::ControladorWiFi(const char* ssid, const char* password)
{
  InicializarNVS();
  std::printf("Iniciando SoftAP...\n");
  s_wifi_event_group = xEventGroupCreate();

  tcpip_adapter_init();
  ESP_ERROR_CHECK(esp_event_loop_init(ciclo_ejecucion_wifi, nullptr));

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  wifi_config_t wifi_config;
  std::strcpy((char*)wifi_config.ap.ssid, ssid);
  wifi_config.ap.ssid_len = std::strlen((char*)wifi_config.ap.ssid);
  wifi_config.ap.channel = 1;
  wifi_config.ap.max_connection = 4;
  std::strcpy((char*)wifi_config.ap.password, password);
  wifi_config.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK;
  if (std::strlen((char*)wifi_config.ap.password) == 0) {
    wifi_config.ap.authmode = WIFI_AUTH_OPEN;
  }

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
  vTaskDelay(pdMS_TO_TICKS(100));
  ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
  ESP_ERROR_CHECK(esp_wifi_start());

  std::printf("SoftAP Iniciado. SSID:%s password:%s canal:%d\n",
              wifi_config.ap.ssid, wifi_config.ap.password,
              wifi_config.ap.channel);
  tcpip_adapter_ip_info_t ip_info;
  tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_AP, &ip_info);
  std::printf("IP: " IPSTR "\n", IP2STR(&ip_info.ip));
}

ControladorWiFi::~ControladorWiFi()
{
  ESP_ERROR_CHECK(esp_wifi_stop());
  ESP_ERROR_CHECK(esp_wifi_deinit());
}

void ControladorWiFi::InicializarNVS()
{
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret); 
}

esp_err_t ControladorWiFi::ciclo_ejecucion_wifi(void* contexto,
                                                system_event_t* evento)
{
  switch(evento->event_id) {
  case SYSTEM_EVENT_AP_STACONNECTED:
    std::printf("Conectada nueva estacion\n");
    break;
  case SYSTEM_EVENT_AP_STADISCONNECTED:
    std::printf("Desconectada estacion\n");
    break;
  default:
    break;
  }
  return ESP_OK;
}