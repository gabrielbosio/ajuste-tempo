#ifndef GENERADOR_DELAY_H
#define GENERADOR_DELAY_H

/**
 * Interfaz para la capa de abstraccion de la funcionalidad de generacion de 
 * delay.
*/
class GeneradorDelay {
public:
  virtual ~GeneradorDelay() {}
  virtual void Esperar(int tiempo_ms) = 0;
};

#endif