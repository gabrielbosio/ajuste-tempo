#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "GeneradorDelayFreeRTOS.h"

GeneradorDelayFreeRTOS::GeneradorDelayFreeRTOS() {}

GeneradorDelayFreeRTOS::~GeneradorDelayFreeRTOS() {}

void GeneradorDelayFreeRTOS::Esperar(int tiempo_ms) {
  vTaskDelay(pdMS_TO_TICKS(tiempo_ms));
}