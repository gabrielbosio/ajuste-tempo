#include "GeneradorDelay.h"

#ifndef GENERADOR_DELAY_FREE_RTOS_H
#define GENERADOR_DELAY_FREE_RTOS_H

/**
 * Capa de abstraccion de la funcionalidad de generacion de delay de FreeRTOS.
*/
class GeneradorDelayFreeRTOS : public GeneradorDelay {
public:
  GeneradorDelayFreeRTOS();
  ~GeneradorDelayFreeRTOS() override;
  /**
   * Genera un delay en la tarea donde se esta llamando este metodo.
   * @param tiempo_ms Tiempo de espera, en milisegundos.
  */
  void Esperar(int tiempo_ms) override;
};

#endif