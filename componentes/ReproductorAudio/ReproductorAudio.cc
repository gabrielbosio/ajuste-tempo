#include "ReproductorAudio.h"

ReproductorAudio::ReproductorAudio(CodecI2S* i2s) : i2s_(i2s) {}

void ReproductorAudio::Reproducir(float* contenido, size_t longitud)
{
  int offset = 0;
  while (offset < longitud) {
    int longitud_reproduccion = longitud_maxima_reproduccion;
    if (longitud_reproduccion > longitud - offset) {
      longitud_reproduccion = longitud - offset;
    }
    i2s_->Escribir(contenido, longitud_reproduccion, offset);
    offset += longitud_reproduccion;
  }
}