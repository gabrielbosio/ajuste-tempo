#ifndef REPRODUCTOR_AUDIO_H
#define REPRODUCTOR_AUDIO_H

#include <cstddef>
#include "CodecI2S.h"

class ReproductorAudio {
public:
  /**
   * Asocia un periferico I2S a este reproductor. 
   * Los datos del audio a reproducir se envian al periferico I2S.
  */
  ReproductorAudio(CodecI2S* i2s);
  /**
   * Envia los datos de contenido al periferico I2S asociado.
   * @param contenido Buffer de datos del audio a reproducir.
   * @param longitud Longitud del buffer.
  */
  void Reproducir(float* contenido, size_t longitud);
private:
  CodecI2S* i2s_;
  static const size_t longitud_maxima_reproduccion = 4 * 1024;
};

#endif