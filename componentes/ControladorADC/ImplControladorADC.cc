#include <cstdlib>
#include <iostream>
#include "ImplControladorADC.h"

ImplControladorADC::ImplControladorADC()
{
  VerificarEFuse();
  ConfigurarADC();
  CaracterizarADC();
}

ImplControladorADC::~ImplControladorADC()
{
  free(caracteristicas_adc);
  caracteristicas_adc = nullptr;
}

float ImplControladorADC::Leer()
{
  uint32_t lectura_adc = 0;
  for (int i = 0; i < cantidad_muestras; i++) {
    if (unidad_adc == ADC_UNIT_1) {
      lectura_adc += adc1_get_raw((adc1_channel_t)canal_adc);
    } else {
      int bruto;
      adc2_get_raw((adc2_channel_t)canal_adc, ADC_WIDTH_BIT_12, &bruto);
      lectura_adc += bruto;
    }
  }
  return lectura_adc / cantidad_muestras / valor_maximo;
}

void ImplControladorADC::VerificarEFuse()
{
  // Verificar si TP esta grabada en eFuse
  if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
    std::cout << "eFuse Two Point: Soportado\n";
  } else {
    std::cout << "eFuse Two Point: NO soportado\n";
  }

  // Verificar si Vref esta grabada en eFuse
  if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
    std::cout << "eFuse Vref: Soportado\n";
  } else {
    std::cout << "eFuse Vref: NO soportado\n";
  }
}

void ImplControladorADC::ConfigurarADC()
{
  if (unidad_adc == ADC_UNIT_1) {
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten((adc1_channel_t)canal_adc, atenuacion_adc);
  } else {
    adc2_config_channel_atten((adc2_channel_t)canal_adc, atenuacion_adc);
  }
}

void ImplControladorADC::CaracterizarADC()
{
  caracteristicas_adc = (esp_adc_cal_characteristics_t*)
                        calloc(1, sizeof(esp_adc_cal_characteristics_t));
  esp_adc_cal_value_t tipo_caracteristica =
    esp_adc_cal_characterize(unidad_adc, atenuacion_adc, ADC_WIDTH_BIT_12,
                             voltaje_referencia, caracteristicas_adc);
  
  if (tipo_caracteristica == ESP_ADC_CAL_VAL_EFUSE_TP) {
    std::cout << "Characterized using Two Point Value\n";
  } else if (tipo_caracteristica == ESP_ADC_CAL_VAL_EFUSE_VREF) {
    std::cout << "Characterized using eFuse Vref\n";
  } else {
    std::cout << "Characterized using Default Vref\n";
  }
}