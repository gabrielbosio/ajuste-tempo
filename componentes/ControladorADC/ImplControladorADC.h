#include <cstddef>
#include <driver/gpio.h>
#include <driver/adc.h>
#include "esp_adc_cal.h"
#include "ControladorADC.h"

#ifndef IMPL_CONTROLADOR_ADC
#define IMPL_CONTROLADOR_ADC

/**
 * Capa de abstraccion del modulo del ADC de ESP-IDF.
*/
class ImplControladorADC : public ControladorADC {
public:
  /**
   * Activa la funcionalidad de lectura usando el ADC.
  */
  ImplControladorADC();
  /**
   * Desactiva la funcionalidad de lectura usando el ADC.
  */
  ~ImplControladorADC() override;
  /**
   * Obtiene un valor de intensidad proveniente del ADC usando la tecninca de 
   * multisampling para reducir el ruido.
   * @return La intensidad de la muestra como un valor de 12 bits (0 - 4095)
  */
  float Leer() override;
private:
  esp_adc_cal_characteristics_t* caracteristicas_adc;
  static const int voltaje_referencia = 1100;
  static const size_t cantidad_muestras = 64UL;
  static constexpr float valor_maximo = 4096.0F;
  static const adc_unit_t unidad_adc = ADC_UNIT_1;
  //ADC1 -> GPIO34, ADC2 -> GPIO14
  static const adc_channel_t canal_adc = ADC_CHANNEL_6;
  static const adc_atten_t atenuacion_adc = ADC_ATTEN_DB_0;
  void VerificarEFuse();
  void ConfigurarADC();
  void CaracterizarADC();
};

#endif