#ifndef CONTROLADOR_ADC_H
#define CONTROLADOR_ADC_H

/**
 * Interfaz para la capa de abstraccion del modulo del ADC.
*/
class ControladorADC {
public:
  virtual ~ControladorADC() {}
  virtual float Leer() = 0;
};

#endif