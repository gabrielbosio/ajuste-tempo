#ifndef SERVIDOR_WEB_H
#define SERVIDOR_WEB_H

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_http_server.h"
#include "LectorSD.h"

/**
 * Conjunto de parametros del metodo POST enviar de ServidorWeb.
*/
struct Contexto {
  /**
   * Referencia a la tarea de ajuste usada para suspender, reanudar o 
   * eliminar esa tarea.
  */
  TaskHandle_t controlador_ajuste;
  /**
   * Referencia a la tarea de reproduccion usada para suspender, reanudar o 
   * eliminar esa tarea.
  */
  TaskHandle_t controlador_reproduccion;
  /**
   * Lector de tarjetas SD usado por ServidorWeb para almacenar, en la 
   * tarjeta SD, la trama de datos que recibe el metodo POST enviar.
  */
  LectorSD* lector_sd;
  /**
   * Tempo actual de la pista de audio a reproducir.
  */
  float tempo;
};

/**
 * Capa de abstraccion para el modulo de servidor web de ESP-IDF.
*/
class ServidorWeb {
public:
  /**
   * Activa la funcionalidad de servidor web. 
   * Inicia un servidor web HTTP con un controlador para el metodo POST 
   * enviar. Este metodo recibe una trama de datos y usa el objeto LectorSD 
   * asociado a contexto para almacenar la trama en la tarjeta SD.
   * @param contexto Conjunto de parametros del metodo POST enviar.
  */
  ServidorWeb(Contexto* contexto);
  /**
   * Desactiva la funcionalidad de servidor web.
  */
  ~ServidorWeb();
private:
  bool iniciado = false;
  httpd_handle_t controlador_servidor = nullptr;
  httpd_uri enviar;
  static esp_err_t ControladorPostEnviar(httpd_req_t* req);
};

#endif