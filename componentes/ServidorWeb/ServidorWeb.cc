#include <cstdio>
#include <esp_log.h>
#include <sdkconfig.h>
#include <sys/param.h>
#include "ServidorWeb.h"

ServidorWeb::ServidorWeb(Contexto* contexto)
{
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();

  std::printf("Iniciando servidor en el puerto: '%d'\n", config.server_port);
  if (httpd_start(&controlador_servidor, &config) == ESP_OK) {
    std::printf("Registrando controladores URI\n");
    enviar = {
      .uri       = "/enviar",
      .method    = HTTP_POST,
      .handler   = ControladorPostEnviar,
      .user_ctx  = (void*)contexto
    };
    httpd_register_uri_handler(controlador_servidor, &enviar);
    iniciado = true;
    std::printf("Servidor iniciado\n");
  } else {
    std::printf("Error al iniciar el servidor!\n");
  }
}

ServidorWeb::~ServidorWeb()
{
  httpd_stop(controlador_servidor);
  controlador_servidor = nullptr;
}

esp_err_t ServidorWeb::ControladorPostEnviar(httpd_req_t* req)
{
  int ret, restante = req->content_len;
  Contexto* contexto = (Contexto*)req->user_ctx;
  vTaskSuspend(contexto->controlador_ajuste);
  vTaskSuspend(contexto->controlador_reproduccion);
  LectorSD* lector_sd = contexto->lector_sd;
  bool escritura_iniciada = lector_sd->IniciarEscritura();

  while (escritura_iniciada && restante > 0) {
    ret = httpd_req_recv(req, (char*)lector_sd->pista_audio(),
                         MIN(restante, lector_sd->longitud_pista()));
    if (ret <= 0) {
      if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
        continue;
      }
      return ESP_FAIL;
    }

    restante -= ret;
    std::printf("==== RECIBIDO (%10d/%10d) ====\n",
                req->content_len - restante, req->content_len);
    if (!lector_sd->Escribir(ret)) {
      break;
    }
  }

  if (!lector_sd->FinalizarEscritura()) {
    return ESP_FAIL;
  }
  contexto->tempo = lector_sd->configuracion()->tempo;
  vTaskResume(contexto->controlador_ajuste);
  vTaskResume(contexto->controlador_reproduccion);
  httpd_resp_send(req, HTTPD_200, strlen(HTTPD_200));

  return ESP_OK;
}