#include "MedidorLatencia.h"

MedidorLatencia::MedidorLatencia(int muestras_segundo,
                                 Cronometro* cronometro) :
  muestras_segundo(muestras_segundo),
  cronometro(cronometro) {}

void MedidorLatencia::Comenzar() {
  cronometro->Comenzar();
}

int64_t MedidorLatencia::Finalizar(int cantidad_muestras) {
  int64_t latencia = -1LL;
  muestras_acumuladas += cantidad_muestras;
  tiempo_acumulado += cronometro->Finalizar();

  if (muestras_acumuladas >= muestras_segundo) {
    muestras_acumuladas = 0;
    latencia = tiempo_acumulado - segundo_us;
    tiempo_acumulado = 0LL;
    if (latencia < 0LL) {
      latencia = 0LL;
    }
  }

  return latencia;
}