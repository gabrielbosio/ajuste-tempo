#ifndef MEDIDOR_LATENCIA_H
#define MEDIDOR_LATENCIA_H

#include <cstdint>
#include "Cronometro.h"

/**
 * Medidor de latencia de procesamiento de audio.
*/
class MedidorLatencia {
public:
  /**
   * Crea un nuevo medidor. 
   * Este medidor supone que una tarea de ajuste de tempo y una tarea de 
   * reproduccion de audio se ejecutan concurrentemente. Si el tiempo que 
   * toma el ajuste de tempo es mayor al tiempo que toma la reproduccion de 
   * audio, este medidor detecta esa diferencia de tiempo como latencia.
   * @param muestras_segundo Cantidad de muestras de audio por segundo.
   * @param cronometro Usado para medir el tiempo de procesamiento.
  */
  MedidorLatencia(int muestras_segundo, Cronometro* cronometro);
  /**
   * Hace comenzar el conteo del cronometro. 
   * Se recomienda llamar este metodo al inicio de la reproduccion de audio.
  */
  void Comenzar();
  /**
   * Hace finalizar el conteo del cronometro.
   * Suma el tiempo transcurrido entre la llamada al metodo Comenzar() y la 
   * llamada a este metodo.
   * Una llamada a este metodo indica el fin del procesamiento de una porcion 
   * de pista de audio de longitud cantidad_muestras.
   * Se recomienda llamar a este metodo al final del procesamiento de audio.
   * @param cantidad_muestras Cantidad de muestras de audio procesadas entre 
   * la llamada al metodo Comenzar().
   * @return La latencia de procesamiento de un segundo de audio si dicho 
   * procesamiento tomo mas de un segundo, 0 si tomo menos de un segundo, o
   * un numetro negativo si todavia no se proceso un segundo de audio.
  */
  int64_t Finalizar(int cantidad_muestras);
private:
  int muestras_segundo;
  int muestras_acumuladas = 0;
  int64_t tiempo_acumulado = 0LL;
  static const int64_t segundo_us = 1000000LL;
  Cronometro* cronometro;
};

#endif