#include <cstring>
#include <iostream>
#include "LectorAudio.h"

LectorAudio::LectorAudio(uint8_t* pista_audio, size_t longitud_pista,
                         size_t longitud_buffer, int holgura,
                         MedidorLatencia* medidor_latencia) :
  pista_audio_(pista_audio),
  longitud_pista_(longitud_pista),
  holgura(holgura),
  longitud_buffers_entrada(longitud_buffer * holgura),
  buffers_entrada(new uint8_t[longitud_buffers_entrada]),
  buffer_entrada_actual_(buffers_entrada),
  longitud_buffer_(longitud_buffer),
  buffer_salida_(new float[longitud_buffer]),
  longitud_lectura_(longitud_buffer / 2),
  longitud_contenido_salida_(longitud_buffer),
  entrada_(pista_audio + longitud_lectura_ * holgura),
  salida_(pista_audio),
  medidor_latencia(medidor_latencia)
{
  VerificarLecturaCompleta();
}

LectorAudio::~LectorAudio()
{
  delete[] buffers_entrada;
  delete[] buffer_salida_;
}

void LectorAudio::Leer()
{
  if (buffer_entrada_listo_) {
    std::memcpy(buffer_entrada_actual_, entrada_, longitud_lectura_);
    entrada_ += longitud_lectura_;
    VerificarLecturaCompleta();
    buffer_entrada_listo_ = false;
    if (medidor_latencia != nullptr) {
      medidor_latencia->Comenzar();
    }
  }
  salida_ += longitud_lectura_;
  if (salida_ >= pista_audio_ + longitud_pista_) {
    salida_ = pista_audio_;
    distancia_pistas--;
  }
  if (salida_ == entrada_ && distancia_pistas == 0) {
    entrada_ += longitud_lectura_ * holgura;
    VerificarLecturaCompleta();
  }
}

void LectorAudio::LlenarBufferSalida(float* contenido, size_t longitud)
{
  if (longitud > longitud_buffer_) {
    longitud = longitud_buffer_;
  }
  std::memcpy(buffer_salida_, contenido, longitud * sizeof(float));
  longitud_contenido_salida_ = longitud;
  buffer_entrada_actual_ += longitud_buffer_;
  if (buffer_entrada_actual_ >= buffers_entrada + longitud_buffers_entrada) {
    buffer_entrada_actual_ = buffers_entrada;
  }
  buffer_entrada_listo_ = true;
  
  if (medidor_latencia != nullptr) {
    int64_t latencia = medidor_latencia->Finalizar(longitud);
    if (latencia >= 0LL) {
      std::cout << "\nLatencia: " << latencia << " us\n";
    }
  }
}

void LectorAudio::NotificarCargaNuevaPista()
{
  leyo_toda_la_pista_ = false;
}

const uint8_t* LectorAudio::entrada() { return entrada_; }

const uint8_t* LectorAudio::salida() { return salida_; }

size_t LectorAudio::longitud_buffer() { return longitud_buffer_; }

size_t LectorAudio::longitud_lectura() { return longitud_lectura_; }

size_t LectorAudio::longitud_contenido_salida() {
  return longitud_contenido_salida_;
}

uint8_t* LectorAudio::buffer_entrada_actual() {
  return buffer_entrada_actual_;
};

float* LectorAudio::buffer_salida() { return buffer_salida_; } ;

bool LectorAudio::buffer_entrada_listo() { return buffer_entrada_listo_; }

bool LectorAudio::leyo_toda_la_pista() { return leyo_toda_la_pista_; }

void LectorAudio::VerificarLecturaCompleta()
{
  int offset_entrada = entrada_ - pista_audio_;
  leyo_toda_la_pista_ = offset_entrada >= longitud_pista_;
  if (leyo_toda_la_pista_) {
    entrada_ = pista_audio_ + offset_entrada % longitud_pista_;
    distancia_pistas++;
  }
}