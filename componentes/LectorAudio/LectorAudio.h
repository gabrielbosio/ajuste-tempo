#ifndef LECTOR_AUDIO_H
#define LECTOR_AUDIO_H

#include <cstdint>
#include <cstddef>
#include "MedidorLatencia.h"

/**
 * Lector circular de audio.
*/
class LectorAudio {
public:
  /**
   * Una lectura de audio implica copiar una porcion de la pista de audio 
   * para ser procesada por otros modulos sin modificar la pista original. 
   * Un lector de audio reserva memoria para almacenar las copias de las 
   * porciones de la pista a leer. Este lector contiene dos punteros a la 
   * pista de audio: uno de entrada y otro de salida. El puntero de entrada 
   * esta adelantado al de salida por una cantidad de bytes establecida por 
   * holgura. Cada puntero tiene asociado un buffer.
   * @param pista_audio Pista de audio a leer.
   * @param longitud_pista Longitud de la pista de audio.
   * @param longitud_buffer Longitud tanto del buffer de entrada como el de 
   * salida.
   * @param holgura Distancia en bytes entre el puntero de entrada y el de
   * salida.
   * @param medidor_latencia Medidor de la latencia generada por el 
   * procesamiento de la pista de audio.
  */
  LectorAudio(uint8_t* pista_audio, size_t longitud_pista,
              size_t longitud_buffer, int holgura,
              MedidorLatencia* medidor_latencia = nullptr);
  /**
   * Libera la memoria reservada para las copias de las porciones de la pista 
   * a leer.
  */
  ~LectorAudio();
  /**
   * Adelanta el puntero de salida.
   * Si el buffer de entrada esta listo para nuevas lecturas, copia la
   * siguiente porcion de la pista de audio al buffer de entrada y adelanta 
   * el puntero de entrada. Si tambien existe un medidor de latencia asociado 
   * a este lector, comienza el conteo del medidor.
  */
  void Leer();
  /**
   * Copia contenido en el buffer de salida.
   * Habilita al buffer de entrada para nuevas lecturas de la pista de audio. 
   * Si existe un medidor de latencia asociado a este lector, finaliza el 
   * conteo del medidor y, cuando este calculada la latencia, la imprime.
  */
  void LlenarBufferSalida(float* contenido, size_t longitud);
  /**
   * La pista de audio a leer es distinta a la que leia este lector antes.
   * Si, leyendo la pista anterior, leyo_toda_la_pista() retornaba true, 
   * ahora leyo_toda_la_pista() retorna false.
  */
  void NotificarCargaNuevaPista();
  /**
   * Puntero a la pista de audio que referencia la proxima porcion de pista a 
   * copiar ante una nueva lectura.
  */
  const uint8_t* entrada();
  /**
   * Puntero a la pista de audio que referencia la posicion de la pista de 
   * audio que deberia reproducirse.
  */
  const uint8_t* salida();
  /**
   * Longitud tanto del buffer de entrada como el de salida.
  */
  size_t longitud_buffer();
  /**
   * Longitud de la porcion de pista de audio a copiar al llamar al metodo 
   * Leer().
  */
  size_t longitud_lectura();
  /**
   * Longitud del contenido almacenado en el buffer de salida. 
   * Este valor se establece al llamar LlenarBufferSalida().
  */
  size_t longitud_contenido_salida();
  /**
   * Buffer donde se almacena las porcion de la pista de audio a leer.
  */
  uint8_t* buffer_entrada_actual();
  /**
   * Buffer donde se almacena la porcion de la pista de audio a reproducir.
  */
  float* buffer_salida();
  /**
   * Retorna true si el buffer de entrada esta listo para nuevas lecturas. De 
   * lo contrario, retorna false.
   * Cada nueva lectura de la pista de audio deshabilita al buffer de 
   * entrada. Cada llenado del buffer de salida habilita al buffer de entrada.
  */
  bool buffer_entrada_listo();
  /**
   * Retorna true si este lector leyo toda la pista de audio. De lo 
   * contrario, retorna false.
  */
  bool leyo_toda_la_pista();
private:
  uint8_t* pista_audio_;
  const size_t longitud_pista_;
  int holgura;
  const size_t longitud_buffers_entrada;
  uint8_t* buffers_entrada;
  uint8_t* buffer_entrada_actual_;
  bool buffer_entrada_listo_ = true;
  const size_t longitud_buffer_;
  float* buffer_salida_;
  const size_t longitud_lectura_;
  size_t longitud_contenido_salida_;
  const uint8_t* entrada_;
  const uint8_t* salida_;
  int distancia_pistas = 0;
  int offset = 0;
  bool leyo_toda_la_pista_;
  MedidorLatencia* medidor_latencia;
  void VerificarLecturaCompleta();
};

#endif