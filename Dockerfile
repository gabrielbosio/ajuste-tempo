FROM espressif/idf:v3.3.3
WORKDIR /ajuste-tempo
COPY . /ajuste-tempo
RUN apt-get update -qy && apt-get install -qy cmake
CMD idf.py fullclean build && cd pruebas_sw && sh ejecutar.sh