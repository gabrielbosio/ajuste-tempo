#include <algorithm>
#include "DetectorTempo.h"
#include "gtest/gtest.h"

TEST(DetectorTempoTest, PicosSeparadosUniformemente)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 600, 1000, 1400};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 150.0F);
}

TEST(DetectorTempoTest, PicosSeparadosConUnPicoMasEntreSeparacionFinal)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {2};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 5;
  int picos[] = {200, 600, 1000, 1200, 1400};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 150.0F);
}

TEST(DetectorTempoTest, PicosSeparadosConUnPicoMasEntreSeparacionInicial)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {2};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 5;
  int picos[] = {200, 400, 600, 1000, 1400};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 150.0F);
}

TEST(DetectorTempoTest, PicosSeparadosConUnPicoMenosEntreSeparacionFinal)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {2};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 3;
  int picos[] = {200, 600, 1400};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 150.0F);
}

TEST(DetectorTempoTest, PicosSeparadosConUnPicoMenosEntreSeparacionInicial)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {2};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 3;
  int picos[] = {200, 1000, 1400};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 150.0F);
}

TEST(DetectorTempoTest, ConPicosMasJuntosElTempoEsMayor)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 500, 800, 1100};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 200.0F);
}

TEST(DetectorTempoTest, ConPicosMasSeparadosElTempoEsMenor)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 700, 1200, 1700};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 120.0F);
}

TEST(DetectorTempoTest, AlEjecutarDetectorSeActualizaTempoReferencia)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {2};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  const size_t cantidad_maxima_picos = 5;
  int picos[cantidad_maxima_picos];
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_maxima_picos);
  
  const size_t cantidad_picos_primera_ejecucion = 4;
  int picos_primera_ejecucion[] = {200, 700, 1200, 1700};
  std::copy(std::begin(picos_primera_ejecucion),
            std::end(picos_primera_ejecucion), std::begin(picos));
  detector.Ejecutar(cantidad_picos_primera_ejecucion);
  const size_t cantidad_picos_segunda_ejecucion = 5;
  int picos_segunda_ejecucion[] = {1700, 1950, 2200, 2700, 3200};
  std::copy(std::begin(picos_segunda_ejecucion),
            std::end(picos_segunda_ejecucion), std::begin(picos));
  float tempo = detector.Ejecutar(cantidad_picos_segunda_ejecucion);

  EXPECT_FLOAT_EQ(tempo, 120.0F);
}

TEST(DetectorTempoTest, ConPicosSeparadosNoUniformementeSeTomaPromedio)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 600, 1200, 1700};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 120.0F);
}

TEST(DetectorTempoTest, EjecutarSinPicosDevuelveTempoNegativo)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 600, 1200, 1700};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(0);

  EXPECT_LE(tempo, 0.0F);
}

TEST(DetectorTempoTest, EjecutarConUnPicoDevuelveTempoNegativo)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 600, 1200, 1700};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(1);

  EXPECT_LE(tempo, 0.0F);
}

TEST(DetectorTempoTest, PicosSeparadosConVariacionMenorAUmbralMinimo)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 20;
  int umbral_polirritmico[] = {};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 590, 1010, 1410};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 150.0F);
}

TEST(DetectorTempoTest, PicosSeparadosFormandoUnTresillo)
{
  float tempo_inicial = 100.0F;
  int umbral_minimo = 0;
  int umbral_polirritmico[] = {3};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 5;
  int picos[] = {200, 800, 1000, 1200, 1400};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 100.0F);
}

TEST(DetectorTempoTest, UmbralPolirritmicoEnCeroNoCuentaEnLaDeteccion)
{
  float tempo_inicial = 150.0F;
  int umbral_minimo = 20;
  int umbral_polirritmico[] = {0};
  size_t longitud_umbral = sizeof(umbral_polirritmico) / sizeof(int);
  size_t cantidad_picos = 4;
  int picos[] = {200, 590, 1010, 1410};
  DetectorTempo detector(tempo_inicial, &umbral_minimo, umbral_polirritmico,
                         longitud_umbral, picos, cantidad_picos);
  
  float tempo = detector.Ejecutar(cantidad_picos);

  EXPECT_FLOAT_EQ(tempo, 150.0F);
}