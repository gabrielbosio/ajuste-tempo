#include <climits>
#include <algorithm>
#include <array>
#include "LectorAudio.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;
using ::testing::ElementsAreArray;

TEST(LectorAudioTest, EnPistaEntradaEstaAdelantadoASalidaSegunHolgura)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);

  EXPECT_EQ(*lector_audio.entrada(), 9);
  EXPECT_EQ(*lector_audio.salida(), 1);
}

TEST(LectorAudioTest, SiEntradaLlegaAlFinalAlAdelantarseEntradaVuelveAInicio)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  size_t longitud_buffer = 8;
  int holgura = 5;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);

  EXPECT_EQ(*lector_audio.entrada(), 5);
}

TEST(LectorAudioTest, SiEntradaLlegaAFinalAlAdelantarseLaPistaEstaLeida)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  size_t longitud_buffer = 8;
  int holgura = 5;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);

  EXPECT_TRUE(lector_audio.leyo_toda_la_pista());
}

TEST(LectorAudioTest, AlCrearLectorElBufferEntradaEstaListo)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);

  EXPECT_TRUE(lector_audio.buffer_entrada_listo());
}

TEST(LectorAudioTest, ConBufferEntradaVacioLeerLlenaMedioBufferConPista)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();

  uint8_t* buffer_entrada = lector_audio.buffer_entrada_actual();
  const size_t longitud_medio_buffer = longitud_buffer / 2;
  std::array<uint8_t, longitud_medio_buffer> buffer_obj;
  std::copy(buffer_entrada, buffer_entrada + longitud_medio_buffer,
            buffer_obj.begin());
  EXPECT_THAT(buffer_obj, ElementsAre(9, 10, 11, 12));
}

TEST(LectorAudioTest, ConBufferEntradaVacioLeerAdelantaEntradaEnPista)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();

  EXPECT_EQ(*lector_audio.entrada(), 13);
}

TEST(LectorAudioTest, ConBufferEntradaVacioYEntradaEnFinalLeerVuelveAlInicio)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 3;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();

  EXPECT_EQ(*lector_audio.entrada(), 1);
}

TEST(LectorAudioTest, LeerHaceQueElBufferEntradaDejeDeEstarListo)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();

  EXPECT_FALSE(lector_audio.buffer_entrada_listo());
}

TEST(LectorAudioTest, ConBufferNoListoLeerNoLlenaOtroBufferEntrada)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();
  lector_audio.Leer();

  uint8_t* buffer_entrada = lector_audio.buffer_entrada_actual();
  const size_t longitud_medio_buffer = longitud_buffer / 2;
  std::array<uint8_t, longitud_medio_buffer> buffer_entrada_obj;
  std::copy(buffer_entrada, buffer_entrada + longitud_medio_buffer,
            buffer_entrada_obj.begin());
  EXPECT_THAT(buffer_entrada_obj, ElementsAre(9, 10, 11, 12));
}

TEST(LectorAudioTest, ConBufferNoListoLeerNoAdelantaEntradaEnPista)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();
  lector_audio.Leer();

  EXPECT_EQ(*lector_audio.entrada(), 13);
}

TEST(LectorAudioTest, LlenarBufferSalidaHaceQueBufferEntradaEsteListo)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  float contenido_buffer_salida[] = {10, 20, 30, 40};
  
  lector_audio.Leer();
  lector_audio.LlenarBufferSalida(contenido_buffer_salida, longitud_buffer);

  float* buffer_salida = lector_audio.buffer_salida();
  const size_t longitud_medio_buffer = longitud_buffer / 2;
  std::array<float, longitud_medio_buffer> buffer_salida_obj;
  std::copy(buffer_salida, buffer_salida + longitud_medio_buffer,
            buffer_salida_obj.begin());
  EXPECT_THAT(buffer_salida_obj, ElementsAreArray(contenido_buffer_salida));
  EXPECT_TRUE(lector_audio.buffer_entrada_listo());
}

TEST(LectorAudioTest, ConUnBufferListoLeerLlenaOtroBufferEntradaConPista)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  float contenido_buffer_salida[] = {10, 20, 30, 40};
  
  lector_audio.Leer();
  uint8_t* primer_buffer_entrada = lector_audio.buffer_entrada_actual();
  lector_audio.LlenarBufferSalida(contenido_buffer_salida, longitud_buffer);
  lector_audio.Leer();
  uint8_t* segundo_buffer_entrada = lector_audio.buffer_entrada_actual();

  const size_t longitud_medio_buffer = longitud_buffer / 2;
  std::array<uint8_t, longitud_medio_buffer> primer_buffer_entrada_obj;
  std::copy(primer_buffer_entrada,
            primer_buffer_entrada + longitud_medio_buffer,
            primer_buffer_entrada_obj.begin());
  std::array<uint8_t, longitud_medio_buffer> segundo_buffer_entrada_obj;
  std::copy(segundo_buffer_entrada,
            segundo_buffer_entrada + longitud_medio_buffer,
            segundo_buffer_entrada_obj.begin());
  EXPECT_THAT(primer_buffer_entrada_obj, ElementsAre(9, 10, 11, 12));
  EXPECT_THAT(segundo_buffer_entrada_obj, ElementsAre(13, 14, 15, 16));
}

TEST(LectorAudioTest, ConBuffersListosLeerAdelantaEntrada)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  float contenido_buffer_salida[] = {10, 20, 30, 40};
  
  lector_audio.Leer();
  lector_audio.LlenarBufferSalida(contenido_buffer_salida, longitud_buffer);
  lector_audio.Leer();

  EXPECT_EQ(*lector_audio.entrada(), 1);
}

TEST(LectorAudioTest, LeerAdelantaSalidaEnPista)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();

  EXPECT_EQ(*lector_audio.salida(), 5);
}

TEST(LectorAudioTest, EntradaSeAdelantaSegunHolguraCuandoEsAlcanzadaPorSalida)
{
  uint8_t pista_audio[] = { 1,  2,  3,  4,
                            5,  6,  7,  8,
                            9, 10, 11, 12,
                           13, 14, 15, 16,
                           17, 18, 19, 20,
                           21, 22, 23, 24};
  const size_t longitud_buffer = 8;
  int holgura = 2;
  LectorAudio lector_audio(pista_audio, sizeof(pista_audio), longitud_buffer,
                           holgura);
  
  lector_audio.Leer();
  lector_audio.Leer();
  lector_audio.Leer();

  EXPECT_EQ(*lector_audio.entrada(), 21);
  EXPECT_EQ(*lector_audio.salida(), 13);
}