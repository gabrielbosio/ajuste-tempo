#include <algorithm>
#include <array>
#include <cstddef>
#include "LectorAudio.h"
#include "BufferAudio.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;

TEST(BufferAudioTest, EntradaEstaAdelantadoAPistaSegunSlack)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  std::array<float, longitud_buffer> entrada_obj;
  std::copy(buffer_audio.entrada(), buffer_audio.entrada() + longitud_buffer,
            entrada_obj.begin());
  std::for_each(entrada_obj.begin(), entrada_obj.end(), [](float &n) {
    n += UINT8_MAX / 2;
  });
  EXPECT_THAT(entrada_obj, ElementsAre(0x09, 0x0A, 0x0B, 0x0C));
}

TEST(BufferAudioTest, CuandoSalidaLlegaAEntradaListaSeCopiaEntradaEnSalida)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  uint8_t nuevo_valor = 0xFF;
  buffer_audio.entrada()[0] = nuevo_valor - UINT8_MAX / 2;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();

  std::array<float, longitud_buffer> salida_obj;
  std::copy(buffer_audio.salida(), buffer_audio.salida() + longitud_buffer,
            salida_obj.begin());
  std::for_each(salida_obj.begin(), salida_obj.end(), [](float &n) {
    n += UINT8_MAX / 2;
  });
  EXPECT_THAT(salida_obj, ElementsAre(nuevo_valor, 0x0A, 0x0B, 0x0C));
}

TEST(BufferAudioTest, CuandoSalidaLlegaAEntradaNoEstaListaLaEntradaSeAdelanta)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  buffer_audio.Actualizar();
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();

  std::array<float, longitud_buffer> entrada_obj;
  std::copy(buffer_audio.entrada(), buffer_audio.entrada() + longitud_buffer,
            entrada_obj.begin());
  std::for_each(entrada_obj.begin(), entrada_obj.end(), [](float &n) {
    n += UINT8_MAX / 2;
  });
  EXPECT_THAT(entrada_obj, ElementsAre(0x05, 0x06, 0x07, 0x08));
}

TEST(BufferAudioTest, ConEntradaListaLaEntradaSigueLeyendoPista)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();

  std::array<float, longitud_buffer> entrada_obj;
  std::copy(buffer_audio.entrada(), buffer_audio.entrada() + longitud_buffer,
            entrada_obj.begin());
  std::for_each(entrada_obj.begin(), entrada_obj.end(), [](float &n) {
    n += UINT8_MAX / 2;
  });
  EXPECT_THAT(entrada_obj, ElementsAre(0x0D, 0x0E, 0x0F, 0x10));
}

TEST(BufferAudioTest, CuandoSalidaLlegaAEntradaListaEntradaSigueLeyendoPista)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  buffer_audio.Actualizar();
  buffer_audio.Actualizar();
  buffer_audio.entrada()[0] = 10.0;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();

  std::array<float, longitud_buffer> entrada_obj;
  std::copy(buffer_audio.entrada(), buffer_audio.entrada() + longitud_buffer,
            entrada_obj.begin());
  std::for_each(entrada_obj.begin(), entrada_obj.end(), [](float &n) {
    n += UINT8_MAX / 2;
  });
  EXPECT_THAT(entrada_obj, ElementsAre(0x0D, 0x0E, 0x0F, 0x10));
}

TEST(BufferAudioTest, ActualizarHaceQueLaEntradaDejeDeEstarLista)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();

  EXPECT_FALSE(buffer_audio.entrada_lista());
}

TEST(BufferAudioTest, SiSalidaLlegoAEntradaListaSalidaLeeLoQueEraEntrada)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  buffer_audio.entrada()[0] = 10.0;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();
  uint8_t nuevo_valor = 0xFF;
  buffer_audio.entrada()[0] = nuevo_valor - UINT8_MAX / 2;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();
  buffer_audio.entrada()[0] = 30.0;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();

  std::array<float, longitud_buffer> salida_obj;
  std::copy(buffer_audio.salida(), buffer_audio.salida() + longitud_buffer,
            salida_obj.begin());
  std::for_each(salida_obj.begin(), salida_obj.end(), [](float &n) {
    n += UINT8_MAX / 2;
  });
  EXPECT_THAT(salida_obj, ElementsAre(nuevo_valor, 0x0E, 0x0F, 0x10));
}

TEST(BufferAudioTest, SiSalidaLlegoAFinalDeEntradaListaEntoncesLeeDeEntrada)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10,
                           0x11, 0x12, 0x13, 0x14,
                           0x15, 0x16, 0x17, 0x18};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 4;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  buffer_audio.entrada()[0] = 10.0;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();
  buffer_audio.entrada()[0] = 20.0;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();
  buffer_audio.Actualizar();
  uint8_t nuevo_valor = 0xFF;
  buffer_audio.entrada()[0] = nuevo_valor - UINT8_MAX / 2;
  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();

  std::array<float, longitud_buffer> salida_obj;
  std::copy(buffer_audio.salida(), buffer_audio.salida() + longitud_buffer,
            salida_obj.begin());
  std::for_each(salida_obj.begin(), salida_obj.end(), [](float &n) {
    n += UINT8_MAX / 2;
  });
  EXPECT_THAT(salida_obj, ElementsAre(nuevo_valor, 0x12, 0x13, 0x14));
}

TEST(BufferAudioTest, LlegarAFinalDeAudio)
{
  uint8_t pista_audio[] = {0x01, 0x02, 0x03, 0x04,
                           0x05, 0x06, 0x07, 0x08,
                           0x09, 0x0A, 0x0B, 0x0C,
                           0x0D, 0x0E, 0x0F, 0x10};
  LectorAudio lector_entrada(pista_audio, sizeof(pista_audio));
  LectorAudio lector_salida(pista_audio, sizeof(pista_audio));
  const size_t longitud_buffer = 4;
  int slack = 2;
  BufferAudio buffer_audio(longitud_buffer, slack, lector_entrada,
                           lector_salida);

  buffer_audio.entrada_lista(true);
  buffer_audio.Actualizar();

  EXPECT_TRUE(buffer_audio.lectura_esta_completa());
}