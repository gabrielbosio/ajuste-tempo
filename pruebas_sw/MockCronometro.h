#include "Cronometro.h"
#include "gmock/gmock.h"

#ifndef MOCK_CRONOMETRO_H
#define MOCK_CRONOMETRO_H

class MockCronometro : public Cronometro {
public:
  ~MockCronometro() {};
  MOCK_METHOD(void, Comenzar, (), (override));
  MOCK_METHOD(int64_t, Finalizar, (), (override));
};

#endif