#include <algorithm>
#include <array>
#include "Desempaquetador.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;
using ::testing::IsNull;

TEST(DesempaquetadorTest, DesempaquetarBufferConfiguraTempo)
{
  uint8_t buffer[] = {1, 2, 3, 30, 0, 100, 20, 50};
  Configuracion configuracion;
  Desempaquetador desempaquetador;

  desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);
  
  EXPECT_EQ(configuracion.tempo, 123.0F);
}

TEST(DesempaquetadorTest, DesempaquetarBufferConfiguraPunteroPista)
{
  uint8_t buffer[] = {1, 2, 3, 30, 0, 100, 20, 50};
  Configuracion configuracion;
  Desempaquetador desempaquetador;

  desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);

  const size_t longitud_pista = 3;
  std::array<uint8_t, longitud_pista> pista_obj;
  std::copy(configuracion.pista, configuracion.pista + longitud_pista,
            pista_obj.begin());
  EXPECT_THAT(pista_obj, ElementsAre(100, 20, 50));
}

TEST(DesempaquetadorTest, DesempaquetarBufferConfiguraUmbralMinimo)
{
  uint8_t buffer[] = {1, 2, 3, 30, 0, 100, 20, 50};
  Configuracion configuracion;
  Desempaquetador desempaquetador;

  desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);

  EXPECT_EQ(configuracion.umbral_minimo, 30);
}

TEST(DesempaquetadorTest, DesempaquetarBufferConUmbralPolirritmicoDesactivado)
{
  uint8_t buffer[] = {1, 2, 3, 30, 0, 100, 20, 50};
  Configuracion configuracion;
  Desempaquetador desempaquetador;

  desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);

  EXPECT_THAT(configuracion.umbral_polirritmico, ElementsAre(2, 0, 4, 0));
}

TEST(DesempaquetadorTest, DesempaquetarBufferConUmbralPolirritmicoActivado)
{
  uint8_t buffer[] = {1, 2, 3, 30, 1, 100, 20, 50};
  Configuracion configuracion;
  Desempaquetador desempaquetador;

  desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);

  EXPECT_THAT(configuracion.umbral_polirritmico, ElementsAre(2, 3, 4, 6));
}

TEST(DesempaquetadorTest, ConBufferMuyCortoElDesempaquetadoFalla)
{
  uint8_t buffer[] = {1, 2, 3, 30};
  Configuracion configuracion;
  Desempaquetador desempaquetador;

  bool desempaquetado_exitoso =
    desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);
  
  EXPECT_FALSE(desempaquetado_exitoso);
}

TEST(DesempaquetadorTest, DesempaquetarBufferSoloConDatosDeConfiguracion)
{
  uint8_t buffer[] = {1, 2, 3, 30, 0};
  Configuracion configuracion;
  configuracion.pista = nullptr;
  Desempaquetador desempaquetador;

  desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);
  
  EXPECT_THAT(configuracion.pista, IsNull());
}

TEST(DesempaquetadorTest, ConTempoConDigitoMayorANueveElDesempaquetadoFalla)
{
  uint8_t buffer[] = {1, 10, 3, 30, 0, 100, 20, 50};
  Configuracion configuracion;
  Desempaquetador desempaquetador;

  bool desempaquetado_exitoso =
    desempaquetador.Desempaquetar(buffer, sizeof(buffer), &configuracion);

  EXPECT_FALSE(desempaquetado_exitoso);
}