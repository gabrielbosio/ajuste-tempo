#include "DetectorPico.h"
#include "gtest/gtest.h"

TEST(DetectorPicoTest, DadaMuestraConIntensidadPicoSeGuardaInstanteMuestra)
{
  size_t cantidad_maxima_picos = 4;
  int picos[cantidad_maxima_picos];
  float intensidad_pico = 0.9;
  int periodo_muestreo = 100;
  int tiempo_maximo = 1000;
  DetectorPico detector(intensidad_pico, picos, cantidad_maxima_picos);
  float muestra_normal = 0.5;
  float muestra_pico = 1.0;

  detector.Ejecutar(muestra_normal, 0);
  detector.Ejecutar(muestra_pico, periodo_muestreo);
  
  EXPECT_EQ(picos[0], periodo_muestreo);
  EXPECT_EQ(detector.cantidad_picos(), 1);
}

TEST(DetectorPicoTest, DosPicosConsecutivosSeDetectanComoUnSoloPico)
{
  size_t cantidad_maxima_picos = 4;
  int picos[cantidad_maxima_picos];
  float intensidad_pico = 0.9;
  int periodo_muestreo = 100;
  int tiempo_maximo = 1000;
  DetectorPico detector(intensidad_pico, picos, cantidad_maxima_picos);
  float muestra_pico = 1.0;

  detector.Ejecutar(muestra_pico, 0);
  detector.Ejecutar(muestra_pico, periodo_muestreo);

  EXPECT_EQ(picos[0], 0);
  EXPECT_EQ(detector.cantidad_picos(), 1);
}

TEST(DetectorPicoTest, ConCantidadPicosMenorALaMaximaLaEjecucionNoEstaLista)
{
  size_t cantidad_maxima_picos = 4;
  int picos[cantidad_maxima_picos];
  float intensidad_pico = 0.9;
  int periodo_muestreo = 100;
  int tiempo_maximo = 1000;
  DetectorPico detector(intensidad_pico, picos, cantidad_maxima_picos);
  float muestra_normal = 0.5;
  float muestra_pico = 1.0;

  detector.Ejecutar(muestra_pico, periodo_muestreo);
  bool esta_lista = detector.Ejecutar(muestra_pico, periodo_muestreo * 2);
  
  EXPECT_FALSE(esta_lista);
}

TEST(DetectorPicoTest, ConCantidadPicosIgualALaMaximaLaEjecucionEstaLista)
{
  size_t cantidad_maxima_picos = 3;
  int picos[cantidad_maxima_picos];
  float intensidad_pico = 0.9;
  int periodo_muestreo = 100;
  int tiempo_maximo = 1000;
  DetectorPico detector(intensidad_pico, picos, cantidad_maxima_picos);
  float muestra_normal = 0.5;
  float muestra_pico = 1.0;

  detector.Ejecutar(muestra_pico, periodo_muestreo);
  detector.Ejecutar(muestra_normal, periodo_muestreo * 2);
  detector.Ejecutar(muestra_pico, periodo_muestreo * 3);
  detector.Ejecutar(muestra_normal, periodo_muestreo * 4);
  bool esta_lista = detector.Ejecutar(muestra_pico, periodo_muestreo * 5);
  
  EXPECT_TRUE(esta_lista);
  EXPECT_EQ(detector.cantidad_picos(), cantidad_maxima_picos);
}

TEST(DetectorPicoTest, ConCantidadMaximaPicosEjecutarReseteaCantidad)
{
  size_t cantidad_maxima_picos = 2;
  int picos[cantidad_maxima_picos];
  float intensidad_pico = 0.9;
  int periodo_muestreo = 100;
  int tiempo_maximo = 1000;
  DetectorPico detector(intensidad_pico, picos, cantidad_maxima_picos);
  float muestra_normal = 0.5;
  float muestra_pico = 1.0;

  detector.Ejecutar(muestra_pico, periodo_muestreo);
  detector.Ejecutar(muestra_normal, periodo_muestreo * 2);
  detector.Ejecutar(muestra_pico, periodo_muestreo * 3);
  detector.Ejecutar(muestra_normal, periodo_muestreo * 4);
  
  EXPECT_EQ(detector.cantidad_picos(), 0);
}