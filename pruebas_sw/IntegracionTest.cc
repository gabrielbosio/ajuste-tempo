#include <random>
#include "DetectorPico.h"
#include "DetectorTempo.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::Return;

TEST(IntegracionTest, DeteccionTempoUnaMuestraDevuelveNumeroNegativo)
{
  float intensidad_pico = 0.9;
  size_t cantidad_maxima_picos = 8;
  int picos[cantidad_maxima_picos];
  DetectorPico detector_pico(intensidad_pico, picos, cantidad_maxima_picos);
  float tempo_inicial = 120.0;
  int umbral_minimo = 0;
  size_t longitud_umbral = 3;
  int umbral_polirritmico[] = {1, 2, 4};
  DetectorTempo detector_tempo(tempo_inicial, &umbral_minimo,
                               umbral_polirritmico, longitud_umbral, picos,
                               cantidad_maxima_picos);
  std::default_random_engine generador_random;
  std::uniform_real_distribution<float> distribucion(0.0, 1.0);

  float muestra = distribucion(generador_random);
  int instante_muestra = detector_pico.Ejecutar(muestra, 0);
  float tempo = detector_tempo.Ejecutar(instante_muestra);

  EXPECT_FLOAT_EQ(tempo, -1.0);
}

TEST(IntegracionTest, DeteccionTempoAudioConDosPicosDevuelveTempo)
{
  float intensidad_pico = 0.9;
  int periodo_muestreo = 100;
  size_t cantidad_maxima_picos = 2;
  int picos[cantidad_maxima_picos];
  DetectorPico detector_pico(intensidad_pico, picos, cantidad_maxima_picos);
  float tempo_inicial = 120.0;
  int umbral_minimo = 0;
  size_t longitud_umbral = 3;
  int umbral_polirritmico[] = {1, 2, 4};
  DetectorTempo detector_tempo(tempo_inicial, &umbral_minimo,
                               umbral_polirritmico, longitud_umbral, picos,
                               cantidad_maxima_picos);
  float audio[] = {0.1, 1.0, 0.2, 0.3, 1.0, 0.1};

  int tiempo_ms = 0;
  float tempo;
  for (float muestra : audio) {
    bool detector_listo = detector_pico.Ejecutar(muestra, tiempo_ms);
    if (detector_listo) {
      tempo = detector_tempo.Ejecutar(detector_pico.cantidad_picos());
    }
    tiempo_ms += periodo_muestreo;
  }

  EXPECT_FLOAT_EQ(tempo, 200.0);
}