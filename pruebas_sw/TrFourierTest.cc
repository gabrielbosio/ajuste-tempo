#include <algorithm>
#include <array>
#include "TrFourier.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;
using ::testing::FloatEq;

// Valores obtenidos de numpy.fft.rfft(...)

TEST(TrFourierTest, EjecutarFFT)
{
  const size_t longitud_espectro = 8;
  float entrada[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0};
  float salida[longitud_espectro];
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  TrFourier fft(longitud_espectro, tabla_senos, longitud_tabla, TipoFFT::real);
  fft.entrada(entrada);
  fft.salida(salida);
  
  fft.Ejecutar();

  std::array<float, longitud_espectro> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatEq(36.0), FloatEq(-4.0),
    FloatEq(-4.0), FloatEq(9.65685425),
    FloatEq(-4.0), FloatEq(4.0),
    FloatEq(-4.0), FloatEq(1.65685425)
  ));
}

TEST(TrFourierTest, EjecutarIFFT)
{
  const size_t longitud_espectro = 8;
  float entrada[] = {
    3.0, 1.0,
    3.45521375, 0.86380344,
    3.63803438, 1.940285,
    2.48507125, 0.62126781
  };
  float salida[longitud_espectro];
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  TrFourier fft(longitud_espectro, tabla_senos, longitud_tabla, TipoFFT::real);
  fft.direccion(DireccionFFT::atras);
  fft.entrada(entrada);
  fft.salida(salida);
  
  fft.Ejecutar();

  std::array<float, longitud_espectro> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatEq(2.89457984), FloatEq(-0.32609865),
    FloatEq(-0.4701425), FloatEq(0.30104668),
    FloatEq(-0.0755626), FloatEq(-0.14404385),
    FloatEq(-0.34887469), FloatEq(1.16909582)
  ));
}