#include <algorithm>
#include <array>
#include <cstring>
#include "GestorVentanas.h"
#include "TrFourier.h"
#include "PhaseVocoder.h"
#include "EscaladorTiempo.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;
using ::testing::FloatNear;

// Valores obtenidos del escalador de ajuste-tempo-sw

TEST(EscaladorTiempoTest, EjecutarConRatioUno)
{
  const size_t longitud_audio = 16;
  uint8_t entrada[] = {1, 2, 3, 4, 3, 2, 1, 2,
                       3, 4, 3, 2, 1, 2, 3, 4};
  std::for_each(std::begin(entrada), std::end(entrada), [=](uint8_t& n)
  {
    n += UINT8_MAX / 2;
  });
  float salida[longitud_audio];
  std::memset(salida, 0, longitud_audio * sizeof(float));
  const size_t longitud_ventana = 8;
  size_t longitud_tabla = 4096;
  auto tabla_senos = new float[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  TrFourier fft(longitud_ventana, tabla_senos, longitud_tabla, TipoFFT::real);
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla);
  EscaladorTiempo escalador(longitud_audio, &gestor_ventanas, &fft,
                            &phase_vocoder);
  
  escalador.Ejecutar(salida, entrada, longitud_audio, longitud_audio);

  std::array<float, longitud_audio> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.01;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(2.0, delta),
    FloatNear(3.0, delta), FloatNear(4.0, delta),
    FloatNear(3.0, delta), FloatNear(2.0, delta),
    FloatNear(1.0, delta), FloatNear(2.0, delta),
    
    FloatNear(3.0, delta), FloatNear(4.0, delta),
    FloatNear(3.0, delta), FloatNear(2.0, delta),
    FloatNear(1.0, delta), FloatNear(2.0, delta),
    FloatNear(3.0, delta), FloatNear(4.0, delta)
  ));
  delete[] tabla_senos;
}

TEST(EscaladorTiempoTest, EjecutarConRatioDos)
{
  const size_t longitud_entrada = 16;
  uint8_t entrada[] = {1, 2, 3, 4, 3, 2, 1, 2,
                       3, 4, 3, 2, 1, 2, 3, 4};
  std::for_each(std::begin(entrada), std::end(entrada), [=](uint8_t& n)
  {
    n += UINT8_MAX / 2;
  });
  const size_t longitud_salida = 8;
  float salida[longitud_salida];
  std::memset(salida, 0, longitud_salida * sizeof(float));
  const size_t longitud_ventana = 8;
  size_t longitud_tabla = 4096;
  auto tabla_senos = new float[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  TrFourier fft(longitud_ventana, tabla_senos, longitud_tabla, TipoFFT::real);
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla);
  EscaladorTiempo escalador(longitud_entrada, &gestor_ventanas, &fft,
                            &phase_vocoder);
  
  escalador.Ejecutar(salida, entrada, longitud_salida, longitud_entrada);

  std::array<float, longitud_salida> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.01;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(2.0, delta),
    FloatNear(3.0, delta), FloatNear(4.0, delta),
    FloatNear(3.0, delta), FloatNear(2.28297, delta),
    FloatNear(2.47467, delta), FloatNear(3.11348, delta)
  ));
  delete[] tabla_senos;
}

TEST(EscaladorTiempoTest, EjecutarConRatioUnMedio)
{
  const size_t longitud_entrada = 8;
  uint8_t entrada[] = {1, 2, 3, 4, 3, 2, 1, 2};
  std::for_each(std::begin(entrada), std::end(entrada), [=](uint8_t& n)
  {
    n += UINT8_MAX / 2;
  });
  const size_t longitud_salida = 16;
  float salida[longitud_salida];
  std::memset(salida, 0, longitud_salida * sizeof(float));
  const size_t longitud_ventana = 8;
  size_t longitud_tabla = 4096;
  auto tabla_senos = new float[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  TrFourier fft(longitud_ventana, tabla_senos, longitud_tabla, TipoFFT::real);
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla);
  EscaladorTiempo escalador(longitud_salida, &gestor_ventanas, &fft,
                            &phase_vocoder);
  
  escalador.Ejecutar(salida, entrada, longitud_salida, longitud_entrada);

  std::array<float, longitud_salida> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.01;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(2.01412, delta),
    FloatNear(2.68015, delta), FloatNear(3.37061, delta),
    FloatNear(2.37913, delta), FloatNear(1.87637, delta),
    FloatNear(2.54179, delta), FloatNear(3.16764, delta),
    
    FloatNear(2.76912, delta), FloatNear(2.28799, delta),
    FloatNear(0.51029, delta), FloatNear(2.62635, delta),
    FloatNear(2.47587, delta), FloatNear(2.08875, delta),
    FloatNear(1.11683, delta), FloatNear(1.69594, delta)
  ));
  delete[] tabla_senos;
}