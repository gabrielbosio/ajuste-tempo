#include "gtest/gtest.h"
#include "LectorAudioTest.cc"
#include "ReproductorAudioTest.cc"
#include "DetectorPicoTest.cc"
#include "DetectorTempoTest.cc"
#include "GestorVentanasTest.cc"
#include "TrFourierTest.cc"
#include "PhaseVocoderTest.cc"
#include "EscaladorTiempoTest.cc"
#include "DesempaquetadorTest.cc"
#include "MedidorLatenciaTest.cc"
#include "IntegracionTest.cc"

int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}