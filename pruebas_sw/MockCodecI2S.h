#include "CodecI2S.h"
#include "gmock/gmock.h"

#ifndef MOCK_CODEC_I2S_H
#define MOCK_CODEC_I2S_H

class MockCodecI2S : public CodecI2S {
public:
  ~MockCodecI2S() {};
  MOCK_METHOD(void, Escribir, (float* origen, size_t longitud, int offset),
              (override));
};

#endif