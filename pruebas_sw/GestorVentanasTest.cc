#include <cstddef>
#include <algorithm>
#include <array>
#include <cstring>
#include "GestorVentanas.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;
using ::testing::FloatEq;
using ::testing::FloatNear;

TEST(GestorVentanasTest, AplicarVentanaAIdentidad)
{
  const size_t longitud_ventana = 8;
  const size_t longitud_audio = 8;
  uint8_t audio[] = {1, 1, 1, 1, 1, 1, 1, 1};
  std::for_each(std::begin(audio), std::end(audio), [](uint8_t& n)
  {
    n += UINT8_MAX / 2;
  });
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  float ventana_audio[longitud_ventana];
  
  int inicio = 0;
  gestor_ventanas.AplicarVentana(ventana_audio, audio, longitud_audio,
                                 inicio);

  std::array<float, longitud_ventana> ventana_audio_obj;
  std::copy(std::begin(ventana_audio), std::end(ventana_audio),
            ventana_audio_obj.begin());
  EXPECT_THAT(ventana_audio_obj, ElementsAre(
    FloatEq(0.0), FloatEq(0.14644661), FloatEq(0.5), FloatEq(0.85355339),
    FloatEq(1.0), FloatEq(0.85355339), FloatEq(0.5), FloatEq(0.14644661)
  ));
}

TEST(GestorVentanasTest, AplicarVentanaAInicioDeAudio)
{
  const size_t longitud_ventana = 8;
  const size_t longitud_audio = 8;
  uint8_t audio[] = {1, 2, 3, 4, 5, 6, 7, 8};
  std::for_each(std::begin(audio), std::end(audio), [](uint8_t& n)
  {
    n += UINT8_MAX / 2;
  });
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  float ventana_audio[longitud_ventana];
  
  int inicio = 0;
  gestor_ventanas.AplicarVentana(ventana_audio, audio, longitud_audio,
                                 inicio);

  std::array<float, longitud_ventana> ventana_audio_obj;
  std::copy(std::begin(ventana_audio), std::end(ventana_audio),
            ventana_audio_obj.begin());
  EXPECT_THAT(ventana_audio_obj, ElementsAre(
    FloatEq(0.0), FloatEq(0.58578644), FloatEq(1.5), FloatEq(1.70710678),
    FloatEq(1.0), FloatEq(1.70710678), FloatEq(1.5), FloatEq(0.58578644)
  ));
}

TEST(GestorVentanasTest, AplicarVentanaAFinDeAudio)
{
  const size_t longitud_ventana = 8;
  const size_t longitud_audio = 8;
  uint8_t audio[] = {1, 2, 3, 4, 5, 6, 7, 8};
  std::for_each(std::begin(audio), std::end(audio), [](uint8_t& n)
  {
    n += UINT8_MAX / 2;
  });
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  float ventana_audio[longitud_ventana];
  
  int fin = 2;
  gestor_ventanas.AplicarVentana(ventana_audio, audio, longitud_audio, fin);

  std::array<float, longitud_ventana> ventana_audio_obj;
  std::copy(std::begin(ventana_audio), std::end(ventana_audio),
            ventana_audio_obj.begin());
  float delta = 0.00001;
  EXPECT_THAT(ventana_audio_obj, ElementsAre(
    FloatNear(0.0, delta), FloatNear(0.87867966, delta),
    FloatNear(3.5, delta), FloatNear(6.82842712, delta),
    FloatNear(7.0, delta), FloatNear(5.12132034, delta),
    FloatNear(2.5, delta), FloatNear(0.58578644, delta)
  ));
}

TEST(GestorVentanasTest, UnirInicioAudio)
{
  const size_t longitud_ventana = 8;
  const size_t longitud_audio = 8;
  float ventana_audio[] = {2.75, -0.60355339, -0.25, 0.10355339,
                           -0.25, 0.10355339, -0.25, -0.60355339};
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  float audio[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  
  int inicio = 0;
  gestor_ventanas.UnirAudio(audio, ventana_audio, longitud_audio, inicio);

  std::array<float, longitud_audio> audio_obj;
  std::copy(std::begin(audio), std::end(audio), audio_obj.begin());
  EXPECT_THAT(audio_obj, ElementsAre(
    FloatEq(-0.25), FloatEq(0.08838835),
    FloatEq(-0.125), FloatEq(-0.08838835),
    FloatEq(0.0), FloatEq(0.0),
    FloatEq(0.0), FloatEq(0.0)
  ));
}

TEST(GestorVentanasTest, UnirFinAudio)
{
  const size_t longitud_ventana = 8;
  const size_t longitud_audio = 8;
  float ventana_audio[] = {1.25, 0.60355339, 0.25, -0.10355339,
                           0.25, -0.10355339, 0.25, 0.60355339};
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);
  float audio[] = {-0.25, 0.1767767, -0.25, -0.1767767,
                   -0.25, -0.08838835, -0.125, 0.08838835};
  
  int fin = 2;
  gestor_ventanas.UnirAudio(audio, ventana_audio, longitud_audio, fin);

  std::array<float, longitud_audio> audio_obj;
  std::copy(std::begin(audio), std::end(audio), audio_obj.begin());
  float delta = 0.0000001;
  EXPECT_THAT(audio_obj, ElementsAre(
    FloatNear(-0.25, delta), FloatNear(0.1767767, delta),
    FloatNear(-0.25, delta), FloatNear(-0.1767767, delta),
    FloatNear(-0.25, delta), FloatNear(0.0, delta),
    FloatNear(0.0, delta), FloatNear(0.0, delta)
  ));
}

TEST(GestorVentanasTest, NormalizarAudio)
{
  const size_t longitud_ventana = 8;
  const size_t longitud_audio = 16;
  float audio[] = {
    0.375, -0.1982233, -0.625, -0.5517767,
    0.375, -0.5, 0.0, -0.5,
    -0.375, -0.5517767, 0.625, -0.1982233,
    -0.375, -0.29105339, 0.75, 0.41605339
  };
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla];
  float dos_pi_sobre_n = 2.0F * M_PI / (longitud_tabla / 2);
  for (int k = 0, m = 0; k < longitud_tabla / 2; k++, m += 2) {
    tabla_senos[m] = cosf(dos_pi_sobre_n * k);
    tabla_senos[m + 1] = sinf(dos_pi_sobre_n * k);
  }
  GestorVentanas gestor_ventanas(longitud_ventana, tabla_senos,
                                 longitud_tabla);

  gestor_ventanas.Normalizar(audio, longitud_audio);

  std::array<float, longitud_audio> audio_obj;
  std::copy(std::begin(audio), std::end(audio), audio_obj.begin());
  EXPECT_THAT(audio_obj, ElementsAre(
    FloatEq(0.375), FloatEq(-0.26429774),
    FloatEq(-1.25), FloatEq(-0.73570226),
    FloatEq(0.375), FloatEq(-0.66666667),
    FloatEq(0.0), FloatEq(-0.66666667),
    FloatEq(-0.375), FloatEq(-0.73570226),
    FloatEq(1.25), FloatEq(-0.26429774),
    FloatEq(-0.375), FloatEq(-0.38807119),
    FloatEq(1.5), FloatEq(0.55473785)
  ));
};