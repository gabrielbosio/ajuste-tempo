#include "MockCronometro.h"
#include "MedidorLatencia.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::AtLeast;
using ::testing::NiceMock;
using ::testing::Return;

TEST(MedidorLatenciaTest, AlIniciarComienzaCronometro)
{
  MockCronometro cronometro;
  EXPECT_CALL(cronometro, Comenzar).Times(1);
  int muestras_segundo = 16000;

  MedidorLatencia medidor(muestras_segundo, &cronometro);

  medidor.Comenzar();
}

TEST(MedidorLatenciaTest, SiNoMarcoUnSegundoDeMuestrasRetornaNegativo)
{
  NiceMock<MockCronometro> cronometro;
  int muestras_segundo = 16000;
  MedidorLatencia medidor(muestras_segundo, &cronometro);

  int cantidad_muestras = 1024;
  medidor.Comenzar();
  int latencia = medidor.Finalizar(cantidad_muestras);

  EXPECT_LE(latencia, 0);
}

TEST(MedidorLatenciaTest, SiMarcoUnSegundoDeMuestrasRetornaLatencia)
{
  int segundo_us = 1000000;
  int latencia_esperada = 500;
  NiceMock<MockCronometro> cronometro;
  EXPECT_CALL(cronometro, Finalizar)
    .WillOnce(Return(segundo_us + latencia_esperada));
  int muestras_segundo = 16000;
  MedidorLatencia medidor(muestras_segundo, &cronometro);

  int cantidad_muestras = 16000;
  medidor.Comenzar();
  int latencia_actual = medidor.Finalizar(cantidad_muestras);

  EXPECT_EQ(latencia_actual, latencia_esperada);
}

TEST(MedidorLatenciaTest, SiNoMarcoOtroSegundoRetornaNegativo)
{
  int segundo_us = 1000000;
  int latencia_esperada = 500;
  NiceMock<MockCronometro> cronometro;
  EXPECT_CALL(cronometro, Finalizar)
    .WillRepeatedly(Return(segundo_us + latencia_esperada));
  int muestras_segundo = 16000;
  MedidorLatencia medidor(muestras_segundo, &cronometro);

  int cantidad_muestras = 8000;
  medidor.Comenzar();
  medidor.Finalizar(cantidad_muestras);
  medidor.Comenzar();
  medidor.Finalizar(cantidad_muestras);
  medidor.Comenzar();
  int latencia = medidor.Finalizar(cantidad_muestras);

  EXPECT_LE(latencia, 0);
}

TEST(MedidorLatenciaTest, SiMarcoUnSegundoEnMenosDeUnSegundoRetornaCero)
{
  int menos_de_un_segundo_us = 500;
  NiceMock<MockCronometro> cronometro;
  EXPECT_CALL(cronometro, Finalizar)
    .WillRepeatedly(Return(menos_de_un_segundo_us));
  int muestras_segundo = 16000;
  MedidorLatencia medidor(muestras_segundo, &cronometro);

  int cantidad_muestras = 16000;
  medidor.Comenzar();
  int latencia = medidor.Finalizar(cantidad_muestras);

  EXPECT_EQ(latencia, 0);
}