#include <cstdint>
#include <cstdlib>
#include "MockCodecI2S.h"
#include "ReproductorAudio.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::AtLeast;
using ::testing::NiceMock;

TEST(ReproductorAudioTest, ReproducirPistaVacia)
{
  NiceMock<MockCodecI2S> i2s;
  EXPECT_CALL(i2s, Escribir).Times(0);

  float contenido_vacio[] = {};
  size_t longitud = sizeof(contenido_vacio);
  ReproductorAudio reproductor (&i2s);
  reproductor.Reproducir(contenido_vacio, longitud);
}

TEST(ReproductorAudioTest, ReproducirPistaConContenido)
{
  NiceMock<MockCodecI2S> i2s;
  EXPECT_CALL(i2s, Escribir).Times(AtLeast(1));
 
  size_t longitud = 4 * 1024;
  float* contenido = new float[longitud];
  ReproductorAudio reproductor (&i2s);
  reproductor.Reproducir(contenido, longitud);
  
  delete[] contenido;
}