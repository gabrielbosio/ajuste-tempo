#include <algorithm>
#include <array>
#include <cstring>
#include "PhaseVocoder.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAre;
using ::testing::FloatNear;

// Valores obtenidos del prototipo de software ajuste-tempo-sw

TEST(PhaseVocoderTest, PhaseVocoderConRatioUnoDebeDejarSalidaIgualAEntrada)
{
  const size_t longitud = 24;
  size_t longitud_ventana = 8;
  size_t salto = 4;
  float entrada[] = {1.0, 0.0, 2.0, 0.0, 2.0, 0.0, 1.0, 0.0,
                     2.0, 0.0, 3.0, 0.0, 3.0, 0.0, 2.0, 0.0,
                     3.0, 0.0, 4.0, 0.0, 4.0, 0.0, 3.0, 0.0};
  float salida[longitud];
  std::memset(salida, 0, longitud * sizeof(float));
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla * 2];
  float two_pi_by_n = 2.0F * M_PI / longitud_tabla;
  for (int k = 0, m = 0; k < longitud_tabla; k++, m += 2) {
    tabla_senos[m] = cosf(two_pi_by_n * k);    // real
    tabla_senos[m + 1] = sinf(two_pi_by_n * k);  // imag
  }
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla * 2);

  float ratio = 1.0;
  phase_vocoder.Ejecutar(salida, entrada, longitud, longitud, ratio,
                         longitud_ventana, salto);

  std::array<float, longitud> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.00001;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(0.0, delta),
    FloatNear(2.0, delta), FloatNear(0.0, delta),
    FloatNear(2.0, delta), FloatNear(0.0, delta),
    FloatNear(1.0, delta), FloatNear(0.0, delta),

    FloatNear(2.0, delta), FloatNear(0.0, delta),
    FloatNear(3.0, delta), FloatNear(0.0, delta),
    FloatNear(3.0, delta), FloatNear(0.0, delta),
    FloatNear(2.0, delta), FloatNear(0.0, delta),

    FloatNear(3.0, delta), FloatNear(0.0, delta),
    FloatNear(4.0, delta), FloatNear(0.0, delta),
    FloatNear(4.0, delta), FloatNear(0.0, delta),
    FloatNear(3.0, delta), FloatNear(0.0, delta)
  ));
}

TEST(PhaseVocoderTest, PhaseVocoderConRatioDosDebeComprimirSalidaALaMitad)
{
  const size_t longitud_entrada = 20;
  size_t longitud_ventana = 4;
  size_t salto = 2;
  float entrada[] = {1.0, 0.0, 2.0, 0.0,
                     3.0, 0.0, 4.0, 0.0,
                     5.0, 0.0, 6.0, 0.0,
                     7.0, 0.0, 8.0, 0.0,
                     9.0, 0.0, 8.0, 0.0};
  const size_t longitud_salida = 12;
  float salida[longitud_salida];
  std::memset(salida, 0, longitud_salida * sizeof(float));
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla * 2];
  float two_pi_by_n = 2.0F * M_PI / longitud_tabla;
  for (int k = 0, m = 0; k < longitud_tabla; k++, m += 2) {
    tabla_senos[m] = cosf(two_pi_by_n * k);    // real
    tabla_senos[m + 1] = sinf(two_pi_by_n * k);  // imag
  }
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla * 2);

  float ratio = 2.0;
  phase_vocoder.Ejecutar(salida, entrada, longitud_salida, longitud_entrada,
                         ratio, longitud_ventana, salto);

  std::array<float, longitud_salida> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.00001;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(0.0, delta),
    FloatNear(2.0, delta), FloatNear(0.0, delta),

    FloatNear(5.0, delta), FloatNear(0.0, delta),
    FloatNear(6.0, delta), FloatNear(0.0, delta),

    FloatNear(9.0, delta), FloatNear(0.0, delta),
    FloatNear(8.0, delta), FloatNear(0.0, delta)
  ));
}

TEST(PhaseVocoderTest, PhaseVocoderConRatioUnMedioDebeExtenderSalidaAlDoble)
{
  const size_t longitud_entrada = 12;
  size_t longitud_ventana = 4;
  size_t salto = 2;
  float entrada[] = {1.0, 0.0, 2.0, 0.0,
                     3.0, 0.0, 4.0, 0.0,
                     5.0, 0.0, 6.0, 0.0};
  const size_t longitud_salida = 24;
  float salida[longitud_salida];
  std::memset(salida, 0, longitud_salida * sizeof(float));
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla * 2];
  float two_pi_by_n = 2.0F * M_PI / longitud_tabla;
  for (int k = 0, m = 0 ; k < longitud_tabla ; k++, m += 2) {
    tabla_senos[m] = cosf(two_pi_by_n * k);    // real
    tabla_senos[m + 1] = sinf(two_pi_by_n * k);  // imag
  }
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla * 2);

  float ratio = 0.5;
  phase_vocoder.Ejecutar(salida, entrada, longitud_salida, longitud_entrada,
                         ratio, longitud_ventana, salto);

  std::array<float, longitud_salida> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.00001;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(0.0, delta),
    FloatNear(2.0, delta), FloatNear(0.0, delta),

    FloatNear(2.0, delta), FloatNear(0.0, delta),
    FloatNear(3.0, delta), FloatNear(0.0, delta),

    FloatNear(3.0, delta), FloatNear(0.0, delta),
    FloatNear(4.0, delta), FloatNear(0.0, delta),

    FloatNear(4.0, delta), FloatNear(0.0, delta),
    FloatNear(5.0, delta), FloatNear(0.0, delta),

    FloatNear(5.0, delta), FloatNear(0.0, delta),
    FloatNear(6.0, delta), FloatNear(0.0, delta),

    FloatNear(2.5, delta), FloatNear(0.0, delta),
    FloatNear(3.0, delta), FloatNear(0.0, delta)
  ));
}

TEST(PhaseVocoderTest, EjecutarPhaseVocoderConRatioUnMedioYNumerosComplejos)
{
  const size_t longitud_entrada = 8;
  size_t longitud_ventana = 4;
  size_t salto = 2;
  float entrada[] = {1.0, 0.0, 2.0, 1.0,
                     3.0, 0.0, 4.0, 1.0};
  const size_t longitud_salida = 16;
  float salida[longitud_salida];
  std::memset(salida, 0, longitud_salida * sizeof(float));
  size_t longitud_tabla = 4096;
  auto tabla_senos = new float[longitud_tabla * 2];
  float two_pi_by_n = 2.0F * M_PI / longitud_tabla;
  for (int k = 0, m = 0; k < longitud_tabla; k++, m += 2) {
    tabla_senos[m] = cosf(two_pi_by_n * k);    // real
    tabla_senos[m + 1] = sinf(two_pi_by_n * k);  // imag
  }
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla * 2);

  float ratio = 0.5;
  phase_vocoder.Ejecutar(salida, entrada, longitud_salida, longitud_entrada,
                         ratio, longitud_ventana, salto);

  std::array<float, longitud_salida> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.01;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(0.0, delta),
    FloatNear(2.0, delta), FloatNear(1.0, delta),

    FloatNear(2.0, delta), FloatNear(0.0, delta),
    FloatNear(3.08465229, delta), FloatNear(0.77116307, delta),

    FloatNear(3.0, delta), FloatNear(0.0, delta),
    FloatNear(4.1216787, delta), FloatNear(0.10846523, delta),
    
    FloatNear(1.5, delta), FloatNear(0.0, delta),
    FloatNear(2.01246118, delta), FloatNear(-0.4472136, delta)
  ));
  delete[] tabla_senos;
}

TEST(PhaseVocoderTest, EjecutarPhaseVocoderConRatioUnMedioYValorFinal)
{
  const size_t longitud_entrada = 12;
  size_t longitud_ventana = 4;
  size_t salto = 2;
  float entrada[] = {1.0, 3.0, 2.0, 0.0,
                     4.0, 6.0, 5.0, 0.0,
                     7.0, 9.0, 8.0, 0.0};
  const size_t longitud_salida = 24;
  float salida[longitud_salida];
  std::memset(salida, 0, longitud_salida * sizeof(float));
  size_t longitud_tabla = 4096;
  float tabla_senos[longitud_tabla * 2];
  float two_pi_by_n = 2.0F * M_PI / longitud_tabla;
  for (int k = 0, m = 0 ; k < longitud_tabla ; k++, m += 2) {
    tabla_senos[m] = cosf(two_pi_by_n * k);    // real
    tabla_senos[m + 1] = sinf(two_pi_by_n * k);  // imag
  }
  PhaseVocoder phase_vocoder(tabla_senos, longitud_tabla * 2);

  float ratio = 0.5;
  phase_vocoder.Ejecutar(salida, entrada, longitud_salida, longitud_entrada,
                         ratio, longitud_ventana, salto);

  std::array<float, longitud_salida> salida_obj;
  std::copy(std::begin(salida), std::end(salida), salida_obj.begin());
  float delta = 0.00001;
  EXPECT_THAT(salida_obj, ElementsAre(
    FloatNear(1.0, delta), FloatNear(3.0, delta),
    FloatNear(2.0, delta), FloatNear(0.0, delta),

    FloatNear(2.5, delta), FloatNear(4.5, delta),
    FloatNear(3.5, delta), FloatNear(0.0, delta),

    FloatNear(4.0, delta), FloatNear(6.0, delta),
    FloatNear(5.0, delta), FloatNear(0.0, delta),

    FloatNear(5.5, delta), FloatNear(7.5, delta),
    FloatNear(6.5, delta), FloatNear(0.0, delta),

    FloatNear(7.0, delta), FloatNear(9.0, delta),
    FloatNear(8.0, delta), FloatNear(0.0, delta),

    FloatNear(3.5, delta), FloatNear(4.5, delta),
    FloatNear(4.0, delta), FloatNear(0.0, delta)
  ));
}